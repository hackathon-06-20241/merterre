import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, Button, StyleSheet} from 'react-native';
import axios from 'axios';
import RNPickerSelect from 'react-native-picker-select';

export const DropDownJSON = ({url, placeholder}) => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    selectedOption: null,
  });
  const [dropdownOptions, setDropdownOptions] = useState([]);

  useEffect(() => {
    // Fetch dropdown options from API
    axios
      .get(url)
      .then(response => {
        setDropdownOptions(
          response.data.map(option => ({
            label: option.libelle || option.id,
            value: option.id,
          })),
        );
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  const handleInputChange = (name, value) => {
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    console.log('Form data submitted:', formData);
    // Handle form submission (e.g., send data to API)
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Selectionner le type de déchets</Text>
      <RNPickerSelect
        onValueChange={value => handleInputChange('selectedOption', value)}
        items={dropdownOptions}
        style={pickerSelectStyles}
        placeholder={{
          label: placeholder,
        }}
      />

      <Button title="Submit" onPress={handleSubmit} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  label: {
    marginVertical: 0,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
    paddingBottom: 20,
  },
});

const pickerSelectStyles = {
  inputIOS: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
  },
  inputAndroid: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
  },
};

export default DropDownJSON;
