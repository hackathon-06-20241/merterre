import React, {useState} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  label: {
    marginVertical: 8,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
  },
});

export const HomeScreen = ({navigation}) => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    selectedOption: null,
  });

  const handleInputChange = (name, value) => {
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  return (
    <View style={styles.sectionContainer}>
      <Text>CONNEXION MER TERRE</Text>

      <Text style={styles.label}>Name</Text>
      <TextInput
        style={styles.input}
        value={formData.name}
        onChangeText={value => handleInputChange('name', value)}
      />

      <Text style={styles.label}>Email</Text>
      <TextInput
        style={styles.input}
        value={formData.email}
        onChangeText={value => handleInputChange('email', value)}
        keyboardType="email-address"
      />

      <Button
        title="Connexion"
        onPress={() => navigation.navigate('Formulaire1')}
      />
    </View>
  );
};
