import {Button, StyleSheet, Text, TextInput, View} from 'react-native';
import DropDownJSON from './DropDownJSON';
import React, {useState} from 'react';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  label: {
    marginVertical: 8,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});
const pickerSelectStyles = {
  inputIOS: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
  },
  inputAndroid: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
  },
};

export const Formulaire1 = ({navigation}) => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    selectedOption: null,
  });

  return (
    <View style={styles.sectionContainer}>
      <Text style={styles.label}>Nom de l'évènement</Text>
      <TextInput style={styles.input} />
      <Text style={styles.label}>Date de l'évenement</Text>
      <TextInput style={styles.input} />
      <Text style={styles.label}>Lieu de l'évenement</Text>
      <TextInput style={styles.input} />
      <Text style={styles.label}>Pays</Text>
      <TextInput style={styles.input} />
      <DropDownJSON
        url="https://hackathon-backend-mt.vercel.app/v1/types/milieu/"
        placeholder="Type de milieu"
      />
      <DropDownJSON
        url="https://hackathon-backend-mt.vercel.app/v1/types/dechet/"
        placeholder="Type de dechet"
      />
      <DropDownJSON
        url="https://hackathon-backend-mt.vercel.app/v1/marques/producteurs/"
        placeholder="Type de Marques"
      />
      <DropDownJSON
        url="https://hackathon-backend-mt.vercel.app/v1/marques/producteurs/"
        placeholder="Type de Marques"
      />
    </View>
  );
};
