"use strict";

var utils = require("../utils/writer.js");
var Releves = require("../service/RelevesService");

module.exports.relevesPOST = function relevesPOST(req, res, next) {
  var body = req.swagger.params["body"].value;
  //console.log(body);
  Releves.relevesPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.relevesGET = function relevesGET(req, res, next) {
  Releves.relevesGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
