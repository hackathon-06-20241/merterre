'use strict';

var utils = require('../utils/writer.js');
var DonneesRef = require('../service/DonneesRefService');

module.exports.basesDechetGET = function basesDechetGET (req, res, next) {
  DonneesRef.basesDechetGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.marquesProducteursGET = function marquesProducteursGET (req, res, next) {
  DonneesRef.marquesProducteursGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.typesDechetGET = function typesDechetGET (req, res, next) {
  DonneesRef.typesDechetGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.typesLieuGET = function typesLieuGET (req, res, next) {
  DonneesRef.typesLieuGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.typesMilieuGET = function typesMilieuGET (req, res, next) {
  DonneesRef.typesMilieuGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
