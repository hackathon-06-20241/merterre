'use strict';

var utils = require('../utils/writer.js');
var Authentification = require('../service/AuthentificationService');

module.exports.loginPOST = function loginPOST (req, res, next) {
  var body = req.swagger.params['body'].value;
  Authentification.loginPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
