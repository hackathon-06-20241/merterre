var MongoClient = require("mongodb").MongoClient;

// Replace the uri string with your MongoDB deployment's connection string.
const uri =
  "mongodb+srv://jcamsler:Bb8nJRxY06RKmM1t@mongodb0.2ed5neo.mongodb.net/";

/**
 * Envoi d'un nouveau releve
 *
 **/
exports.relevesPOST = async function (body) {
  try {
    console.log("Post d'un relevé");
    // Create a new client and connect to MongoDB
    const client = new MongoClient(uri);
    // Connect to the "insertDB" database and access its "haiku" collection
    const database = client.db("MerTerre");
    const releves = database.collection("releves");

    // Insert the defined document into the "haiku" collection
    const result = await releves.insertOne(body);
    // Print the ID of the inserted document
    console.log(`Un relevé a été inséré avec l 'id _id: ${result.insertedId}`);
  } finally {
    // Close the MongoDB client connection
    await client.close();
  }
};

/**
 * Donnes la liste de tous les releves saisis
 * Donnes la liste de tous les releves saisis
 *
 * returns Re
 **/
exports.relevesGET = function () {
  return new Promise(function (resolve, reject) {
    // Create a new client and connect to MongoDB
    const mongoClient = new MongoClient(uri);
    // Connect to the "insertDB" database
    const database = mongoClient.db("MerTerre");
    try {
      const releves = database.collection("releves");
      console.log("Recuperation de tous les releves");
      const docs = releves.find({}).toArray();
      console.log(docs);
      resolve(docs);
    } catch {
      database.close();
      mongoClient.close();
    }
  });
};
