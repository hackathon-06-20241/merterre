'use strict';


/**
 * Service d'authentification pour recuperer un token
 * API for Login
 *
 * body Login Login Payload
 * returns LoginResponse
 **/
exports.loginPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {"empty": false};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

