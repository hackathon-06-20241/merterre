"use strict";

/**
 * Donne la liste de toute la base de dechet (800 ref)
 * Donne la liste de toute la base de dechet
 *
 * returns BaseDechets
 **/
exports.basesDechetGET = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        uuid: "123e4567-e89b-12d3-a456-426614174000",
        value: "Déchet 1",
        data: "2024-06-15",
        thesaurus: "Thesaurus 1",
        description: "Description 1",
        "Materiau Affichage niv3": "Plastique",
        "statut déchet niv2": "Statut 1",
        deleted_at: null,
        Niveau: "Niveau 1",
        "Libellé de regroupement dataviz": "Regroupement 1",
        "Libellé déchet": "Libellé 1",
        Generic: "Générique 1",
        Specific: "Spécifique 1",
        "Code DCSMM": "DCSMM1",
        "Code OSPAR": "OSPAR1",
        TypeCode: "TC1",
        jCode: "JC1",
        "Matériaux DCSMM": "Matériau DCSMM 1",
        "Catégorie DCSMM Plastique": "Catégorie Plastique 1",
        "Matériaux ADEME": "Matériau ADEME 1",
        Usage: "Usage 1",
        "Secteur économique": "Secteur 1",
        REP: "REP1",
      },
      {
        uuid: "123e4567-e89b-12d3-a456-426614174001",
        value: "Déchet 2",
        data: "2024-06-14",
        thesaurus: "Thesaurus 2",
        description: "Description 2",
        "Materiau Affichage niv3": "Caoutchouc",
        "statut déchet niv2": "Statut 2",
        deleted_at: null,
        Niveau: "Niveau 2",
        "Libellé de regroupement dataviz": "Regroupement 2",
        "Libellé déchet": "Libellé 2",
        Generic: "Générique 2",
        Specific: "Spécifique 2",
        "Code DCSMM": "DCSMM2",
        "Code OSPAR": "OSPAR2",
        TypeCode: "TC2",
        jCode: "JC2",
        "Matériaux DCSMM": "Matériau DCSMM 2",
        "Catégorie DCSMM Plastique": "Catégorie Plastique 2",
        "Matériaux ADEME": "Matériau ADEME 2",
        Usage: "Usage 2",
        "Secteur économique": "Secteur 2",
        REP: "REP2",
      },
      {
        uuid: "123e4567-e89b-12d3-a456-426614174002",
        value: "Déchet 3",
        data: "2024-06-13",
        thesaurus: "Thesaurus 3",
        description: "Description 3",
        "Materiau Affichage niv3": "Bois",
        "statut déchet niv2": "Statut 3",
        deleted_at: null,
        Niveau: "Niveau 3",
        "Libellé de regroupement dataviz": "Regroupement 3",
        "Libellé déchet": "Libellé 3",
        Generic: "Générique 3",
        Specific: "Spécifique 3",
        "Code DCSMM": "DCSMM3",
        "Code OSPAR": "OSPAR3",
        TypeCode: "TC3",
        jCode: "JC3",
        "Matériaux DCSMM": "Matériau DCSMM 3",
        "Catégorie DCSMM Plastique": "Catégorie Plastique 3",
        "Matériaux ADEME": "Matériau ADEME 3",
        Usage: "Usage 3",
        "Secteur économique": "Secteur 3",
        REP: "REP3",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Donnes la liste de toutes les marques producteur
 * Donnes la liste de toutes les marques producteurs
 *
 * returns Donnees_ref
 **/
exports.marquesProducteursGET = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      { id: "1664" },
      { id: "Actimel" },
      { id: "Airwaves" },
      { id: "Alvalle" },
      { id: "Andros" },
      { id: "Arbre Magique" },
      { id: "Ariel" },
      { id: "Asics" },
      { id: "Auchan" },
      { id: "Badoit" },
      { id: "Bahlsen" },
      { id: "Balconi" },
      { id: "Balisto" },
      { id: "Benenuts" },
      { id: "BIC" },
      { id: "Bière 86" },
      { id: "Blini" },
      { id: "Blédina" },
      { id: "BN" },
      { id: "Bonduel" },
      { id: "Bounty" },
      { id: "Bouton d'or" },
      { id: "Brioche Dorée" },
      { id: "Brioche Pasquier" },
      { id: "Brossard" },
      { id: "Burger king" },
      { id: "Candia" },
      { id: "Capri-Sun " },
      { id: "Carambar" },
      { id: "Carrefour" },
      { id: "Carte D'or" },
      { id: "Casino" },
      { id: "Chabrior" },
      { id: "Chavroux" },
      { id: "Chupa Chups" },
      { id: "Coca-Cola" },
      { id: "Contrex" },
      { id: "Cornetto" },
      { id: "Corsica Ferries" },
      { id: "Crazy Tiger" },
      { id: "Cristaline" },
      { id: "Crownfield" },
      { id: "Crusti Croc" },
      { id: "Cubanisto" },
      { id: "Cœur de lion" },
      { id: "Daim" },
      { id: "Danone" },
      { id: "Desperados" },
      { id: "Dia" },
      { id: "Doliprane" },
      { id: "Doowap" },
      { id: "Dr Pepper" },
      { id: "Durex" },
      { id: "Eco +" },
      { id: "Evian" },
      { id: "Fanta" },
      { id: "Ferrero" },
      { id: "Findus" },
      { id: "Fini" },
      { id: "Fleury michon" },
      { id: "Floralys" },
      { id: "Fnac" },
      { id: "Française des Jeux" },
      { id: "Freedent" },
      { id: "Galbani" },
      { id: "Gauloise" },
      { id: "Gerblé" },
      { id: "Haagen Daaz" },
      { id: "Haribo" },
      { id: "Harry's" },
      { id: "Hasbro" },
      { id: "Heineken" },
      { id: "Heinz" },
      { id: "Herta" },
      { id: "Hollywood" },
      { id: "Huggies" },
      { id: "Ice Tea" },
      { id: "Ikea" },
      { id: "Ilham" },
      { id: "JB" },
      { id: "Karlsquell" },
      { id: "Kellogg's" },
      { id: "KFC" },
      { id: "Kinder" },
      { id: "KitKat" },
      { id: "Kleenex" },
      { id: "Krema" },
      { id: "Kronenbourg" },
      { id: "L'occitane" },
      { id: "L'Oréal" },
      { id: "La Croix" },
      { id: "La Maison du Café" },
      { id: "La Pie qui Chante" },
      { id: "La Provence" },
      { id: "Label 5" },
      { id: "Lay's" },
      { id: "Leader price" },
      { id: "Leclerc" },
      { id: "Leffe" },
      { id: "Legrand" },
      { id: "Lidl" },
      { id: "Lindt" },
      { id: "Lion" },
      { id: "Lipton Ice Tea" },
      { id: "Listel Rosé" },
      { id: "Look o look" },
      { id: "Lotus" },
      { id: "Lu" },
      { id: "Lustucru" },
      { id: "Lutti" },
      { id: "M&Ms" },
      { id: "McDonald's" },
      { id: "Magnum" },
      { id: "Maitre Jean Pierre" },
      { id: "Malabar" },
      { id: "Marlboro" },
      { id: "Malongo" },
      { id: "Maltesers" },
      { id: "Mamie nova" },
      { id: "Maoam" },
      { id: "Marie Rosé" },
      { id: "Marque Repère" },
      { id: "Mars" },
      { id: "Martens" },
      { id: "Materne" },
      { id: "Menthos" },
      { id: "Miko" },
      { id: "Milka" },
      { id: "Mini bubble" },
      { id: "Minidou" },
      { id: "Minute maid" },
      { id: "MIR" },
      { id: "Monster" },
      { id: "Monster Munch" },
      { id: "Mr Freeze" },
      { id: "Nana" },
      { id: "Nature Addict" },
      { id: "Nestea" },
      { id: "Nestlé" },
      { id: "Netto" },
      { id: "Nutella" },
      { id: "O'Tacos" },
      { id: "Oasis" },
      { id: "Oral B" },
      { id: "Orangina" },
      { id: "Oreo" },
      { id: "Panachade" },
      { id: "Parmareggio" },
      { id: "Paskesz" },
      { id: "Pasquier" },
      { id: "Pâturages" },
      { id: "Pepsi" },
      { id: "Perlembourg" },
      { id: "Perrier" },
      { id: "Peugeot" },
      { id: "Philip Morris" },
      { id: "Plantop delta" },
      { id: "Playmobile" },
      { id: "Poliakov" },
      { id: "Pom Pote" },
      { id: "Powerade" },
      { id: "Pringles" },
      { id: "Propre odeur" },
      { id: "Président" },
      { id: "Pulco" },
      { id: "Quick" },
      { id: "Ramzy Gum" },
      { id: "Redbull" },
      { id: "Ricard" },
      { id: "RichesMonts" },
      { id: "Ricqles" },
      { id: "Rivera system" },
      { id: "Rizzla" },
      { id: "RTM" },
      { id: "Sader" },
      { id: "Saguaro eau de source" },
      { id: "Saint Michel" },
      { id: "San Benedetto" },
      { id: "San Carlo" },
      { id: "San Miguel" },
      { id: "San pellegrino" },
      { id: "Savane" },
      { id: "Saveur de nos Régions" },
      { id: "Schweppes" },
      { id: "Scorpio" },
      { id: "Scotch VHS" },
      { id: "Selecto" },
      { id: "Sibell" },
      { id: "Smarties" },
      { id: "Snickers" },
      { id: "Sodebo" },
      { id: "Solevita" },
      { id: "Sondey" },
      { id: "Sprite" },
      { id: "Stoeffler" },
      { id: "Stoptou" },
      { id: "Subway" },
      { id: "Tous les jours" },
      { id: "Toysrus" },
      { id: "Tropicana" },
      { id: "Tropico" },
      { id: "TUC" },
      { id: "Twix" },
      { id: "Têtes brulées" },
      { id: "U SuperHyper" },
      { id: "Vichy" },
      { id: "Vico" },
      { id: "Vidal" },
      { id: "Vitago" },
      { id: "Vittel" },
      { id: "Volvic" },
      { id: "Waterman" },
      { id: "Werther's Original" },
      { id: "Whaou!" },
      { id: "Whiskas" },
      { id: "Winston" },
      { id: "Yeti" },
      { id: "Yop" },
      { id: "Yoplait" },
      { id: "Zeeman" },
      { id: "Absolute" },
      { id: "After Eight" },
      { id: "Alesto" },
      { id: "Always" },
      { id: "Apéricube" },
      { id: "Auchan Baby" },
      { id: "Auchan Bio" },
      { id: "Baby" },
      { id: "Barilla" },
      { id: "Beaupré" },
      { id: "Bellarom" },
      { id: "Belle France" },
      { id: "Ben & Jerry's" },
      { id: "Bijou Brigitte" },
      { id: "Bio Intermarché" },
      { id: "Bio Village" },
      { id: "Bistro-vite" },
      { id: "Bon appétit" },
      { id: "Bonne Maman" },
      { id: "Bonvalle" },
      { id: "Bout-chou" },
      { id: "Buitoni" },
      { id: "Café Richard" },
      { id: "Carrefour Baby" },
      { id: "Carrefour Bio" },
      { id: "Carrefour Kids" },
      { id: "Carrefour No Gluten" },
      { id: "Carrefour Sélection" },
      { id: "Carrefour Veggie" },
      { id: "Casino Bio" },
      { id: "Casino ça vient d'ici" },
      { id: "Casino d'avenir" },
      { id: "Casino Délices" },
      { id: "Casino Saveur d'ailleurs" },
      { id: "Charles Alice" },
      { id: "Cheetos" },
      { id: "Chef Select" },
      { id: "Chocolaterie Monbana" },
      { id: "Cien" },
      { id: "Clipper" },
      { id: "Club des sommeliers" },
      { id: "Club Sandwich" },
      { id: "Combino" },
      { id: "Companino" },
      { id: "Connetable" },
      { id: "Cora" },
      { id: "Cora Degustation" },
      { id: "Coraya" },
      { id: "Cruesli" },
      { id: "Crunch" },
      { id: "Delacre" },
      { id: "Délichoc" },
      { id: "Disney" },
      { id: "Doritos" },
      { id: "Drizz" },
      { id: "Dulano" },
      { id: "Duplo" },
      { id: "EcoPlanet" },
      { id: "Elodi" },
      { id: "Esprit de fête" },
      { id: "Filière Qualité Carrefour" },
      { id: "Fisher" },
      { id: "Florette" },
      { id: "Food to Go" },
      { id: "Freeway" },
      { id: "Freez" },
      { id: "Friskies" },
      { id: "Fruitma" },
      { id: "Fuzetea" },
      { id: "Garnier" },
      { id: "Gelatelli" },
      { id: "Gervais" },
      { id: "Gifi" },
      { id: "Go Sport" },
      { id: "Goudale" },
      { id: "Grand Frais" },
      { id: "Granola" },
      { id: "Guichard Perrachon" },
      { id: "Gusto de Brio" },
      { id: "Gyma" },
      { id: "Hoegaarden" },
      { id: "Honest" },
      { id: "Hyba" },
      { id: "Innocent" },
      { id: "Intermarché" },
      { id: "Jack Daniels" },
      { id: "Jean Rozé" },
      { id: "Kido" },
      { id: "Klindo" },
      { id: "Koenigsbier" },
      { id: "Kong Strong" },
      { id: "L&M" },
      { id: "La beauté" },
      { id: "La case aux épices" },
      { id: "La mère Poulard" },
      { id: "La Roche Posay" },
      { id: "La villageoise" },
      { id: "Lactel" },
      { id: "L'oiseau" },
      { id: "L'origine du goût" },
      { id: "Lovilio" },
      { id: "Madrange" },
      { id: "Maestade" },
      { id: "Maggi" },
      { id: "Maitre Coq" },
      { id: "Makla El Hilal" },
      { id: "Mandine" },
      { id: "Mesa Bella" },
      { id: "Mieux vivre Bio" },
      { id: "Mikado" },
      { id: "Milbona" },
      { id: "Mmm!" },
      { id: "Mon cheri" },
      { id: "Monique Ranou" },
      { id: "Monop'Make up" },
      { id: "Monoprix" },
      { id: "Monoprix bébé" },
      { id: "Monoprix Bio" },
      { id: "Monoprix bio origines" },
      { id: "Monoprix Fit" },
      { id: "Monoprix Gourmet" },
      { id: "Monoprix Je suis vert" },
      { id: "Mountain Dew" },
      { id: "Mustade" },
      { id: "Nalk & Rey" },
      { id: "Native" },
      { id: "Nature Bio" },
      { id: "Negrita" },
      { id: "Nescafé" },
      { id: "Nos régions ont du talent" },
      { id: "Nougasti" },
      { id: "Nuii" },
      { id: "Nuxe" },
      { id: "Ondine" },
      { id: "Pago" },
      { id: "Panach'" },
      { id: "Panaché" },
      { id: "Paquito" },
      { id: "Patrimoine Gourmand" },
      { id: "Pelforth" },
      { id: "Pez" },
      { id: "Phare d'Eckmühl" },
      { id: "Picard" },
      { id: "Pitch" },
      { id: "Pom'Lisse" },
      { id: "Pommette" },
      { id: "Poss" },
      { id: "Pouce" },
      { id: "Produit de terroir" },
      { id: "P'tit Deli" },
      { id: "Reflets de France" },
      { id: "Rik & Rok" },
      { id: "Rockstar" },
      { id: "Saer Brau" },
      { id: "Salvita" },
      { id: "Saveur et passion" },
      { id: "Select" },
      { id: "Séléction d'experts" },
      { id: "Seven up" },
      { id: "Simpl" },
      { id: "Sincère" },
      { id: "Smartwater" },
      { id: "Snack Day" },
      { id: "Soft Bio" },
      { id: "Source des pins" },
      { id: "St Mamet" },
      { id: "St. Dominique" },
      { id: "Stemelen" },
      { id: "Super U" },
      { id: "Terre & Saveurs" },
      { id: "Thiriet" },
      { id: "Tic-Tac" },
      { id: "Tissaia" },
      { id: "Tourtel" },
      { id: "Tradizioni D'italia" },
      { id: "Ugine" },
      { id: "Vache qui rit" },
      { id: "Vert Azur" },
      { id: "Vertu Food" },
      { id: "Vogue" },
      { id: "Volae" },
      { id: "W5" },
      { id: "Ysiance" },
      { id: "Zenova" },
      { id: "Prix mini" },
      { id: "U" },
      { id: "Corona" },
      { id: "Daddy" },
      { id: "Folliet" },
      { id: "Henry Blanc" },
      { id: "Lavazza" },
      { id: "Nespresso" },
      { id: "Caperlan" },
      { id: "Calippo" },
      { id: "Sweet Corner" },
      { id: "Arlequin" },
      { id: "Daunat" },
      { id: "Gusto" },
      { id: "K-RO" },
      { id: "Janssen-Cilag" },
      { id: "Animal World" },
      { id: "Kim'Play" },
      { id: "Force 4" },
      { id: "Stella Artois" },
      { id: "Milky Way" },
      { id: "Saint Louis" },
      { id: "Elle & Vire" },
      { id: "Country Farm" },
      { id: "Napoléon" },
      { id: "Tempo" },
      { id: "Grimbergen" },
      { id: "Justin Bridou" },
      { id: "Fruitella" },
      { id: "OCB" },
      { id: "Zubrowka" },
      { id: "Ricola" },
      { id: "Fruito" },
      { id: "Docteur Candy" },
      { id: "Decathlon" },
      { id: "Duvel" },
      { id: "Choco Nussa" },
      { id: "St Moret" },
      { id: "Pocket Coffee" },
      { id: "UPSA" },
      { id: "PopFruit" },
      { id: "Ker Cadelac" },
      { id: "Mondelez" },
      { id: "Belin" },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)]);
    } else {
      resolve();
    }
  });
};

/**
 * Donne la liste de tous les types de dechets
 * Donne la liste de tous les types de dechets
 *
 * returns Donnees_ref
 **/
exports.typesDechetGET = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      { id: "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf", libelle: "Echoué" },
      { id: "4b8c42a7-a4f2-4e25-a58e-0e007d0bc932", libelle: "Enfoui" },
      { id: "443c9f2d-da72-42ae-9ff5-8f2facd49b8f", libelle: "Flottant" },
      {
        id: "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
        libelle: "Flottant et Fond",
      },
      { id: "c5c9b6b7-8147-4dba-a11e-64c171ee033e", libelle: "Fond" },
      {
        id: "7c96c580-6985-48bf-b4de-f2cd41509a74",
        libelle: "Présent au sol (abandonné)",
      },
      {
        id: "c7381604-ee86-4dce-9476-1951b1efb391",
        libelle: "Présent au sol (abandonné) et échoué",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Donne la liste de tous les types de dechets
 * Donne la liste de tous les types de dechets
 *
 * returns Donnees_ref
 **/
exports.typesLieuGET = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: "f5d03de6-f3fb-4595-bd68-071daf453953",
        libelle: "Autre espace naturel",
      },
      {
        id: "bb845e39-5543-46db-b835-b42c616ad62e",
        libelle: "Berge naturelle (hors plage)",
      },
      {
        id: "9f98ff48-e509-4d59-976b-98ddec122c6b",
        libelle: "Canaux et Salins",
      },
      {
        id: "f048d0df-1271-4bb2-b21a-c6d58024e4ad",
        libelle: "Champ - prairie - lande - garrigue - maquis",
      },
      {
        id: "fc91294b-445b-4610-addd-1884f7b4c3ca",
        libelle: "Champ et prairie",
      },
      {
        id: "56ef88d1-a127-4312-b4fc-e97804a756b3",
        libelle: "Digue et ouvrage",
      },
      { id: "5a5b9691-71eb-4b85-911d-a4f70576b5ac", libelle: "Dune" },
      {
        id: "6f482aa4-fc6e-4efc-9955-3eb14d881775",
        libelle: "Espace naturel en arrière plage/côte (calanques, colline …)",
      },
      { id: "df6eb7ca-bc7a-42b3-9bb1-fe94c619b27f", libelle: "Forêt" },
      {
        id: "af5b532d-e85f-41c3-8370-a36bc53e4d51",
        libelle: "Lit du cours d'eau (fond et/ou surface)",
      },
      { id: "6ce08387-ba56-4147-b2e0-7dcd7630bc1c", libelle: "Mangrove" },
      { id: "3061c0a1-abc6-474b-b219-2a55879ed482", libelle: "Mer - Océan" },
      { id: "8e047d6c-f1cb-4885-b223-60d7ee416c34", libelle: "Multi-lieux" },
      {
        id: "794a1ace-8ef3-4c28-b3cd-9ee8673b889e",
        libelle: "Parc urbain - Jardin public",
      },
      { id: "7c099b82-5a3a-42da-adc2-acc909023b93", libelle: "Parking" },
      { id: "f8c597eb-6a72-40f5-8a11-63ace1adb75f", libelle: "Piste de ski" },
      {
        id: "4214888a-8f13-428c-8371-23084b7fee3e",
        libelle: "Plage (sable, galets, gravillons)",
      },
      {
        id: "950570b8-1dd3-4a6e-97ef-87d94b501db7",
        libelle: "Plan d'eau (fond et/ou surface)",
      },
      { id: "459dd694-d67a-4a05-8f52-ea85a8195edc", libelle: "Port" },
      { id: "cb7517d1-e63f-499d-9016-8499cb5b6ae1", libelle: "Port - barrage" },
      {
        id: "c853cac4-2c1f-4f0f-8919-113ab122abe6",
        libelle: "Port - barrage - écluse",
      },
      {
        id: "bbb12717-2e41-440c-b45b-bc2d2f39321b",
        libelle: "Quai, Digue et ouvrage",
      },
      {
        id: "3be65b46-97af-485b-8bf3-cbef5e8fb4e9",
        libelle: "Rocher et crique",
      },
      { id: "791cc753-9ddb-4b51-838f-fec661ac8841", libelle: "Route" },
      {
        id: "d52ba08d-ef95-40aa-93df-ad8590a398f5",
        libelle: "Route, rue, place",
      },
      {
        id: "774d7a3a-bd7d-474e-a327-4e9bc0f8f352",
        libelle: "Sentier et chemin",
      },
      { id: "fb9de3f5-ff7e-4199-8a53-fb34d9f4ada6", libelle: "Station de ski" },
      {
        id: "cbcddc23-6c92-42e9-9a78-fa93e0e6f24d",
        libelle: "Voie de chemin de fer",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Donnes la liste de tous les types de dechets
 * Donne la liste de tous les types de dechets
 *
 * returns Donnees_ref
 **/
exports.typesMilieuGET = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      { id: "0e6c0232-dd31-4ad2-9617-d13485480ef9", libelle: "Cours d'eau" },
      { id: "15f80e5b-6e91-416d-a509-6c98f0aa29cf", libelle: "Lac et Marais" },
      {
        id: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
        libelle: "Lagune et étang côtier",
      },
      {
        id: "08b669e5-c807-40d3-806e-5d3191c09dfc",
        libelle: "Littoral (terrestre)",
      },
      { id: "014539f4-b8b0-4a9c-a209-993a13a147aa", libelle: "Mer - Océan" },
      { id: "2985e147-53eb-48ff-aea9-9f2126d0ce83", libelle: "Montagne" },
      { id: "2c7b9fe3-b729-4577-b128-790a48d4d890", libelle: "Multi-lieux" },
      {
        id: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
        libelle: "Zone naturelle ou rurale (hors littoral et montagne)",
      },
      { id: "167b5f65-221c-40c5-afc2-d3e397738691", libelle: "Zone urbaine" },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};
