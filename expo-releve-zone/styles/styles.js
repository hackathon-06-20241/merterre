import { StyleSheet } from "react-native";

export const form_back_color = "#ddd";
export const mer_terre_blue_color = "#062f65";

const s = StyleSheet.create({
  page_title: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: mer_terre_blue_color,
    borderRadius: 5,
  },

  titre_h1: {
    fontSize: 25,
    padding: 5,
    alignSelf: "center",
    width: "100%",
    color: "white",
    fontWeight: "bold",
    marginBottom: 5,
  },

  titre_h2: {
    fontSize: 20,
    paddingBottom: 10,
    paddingTop: 10,
  },

  txt_small: {
    fontSize: 12,
    paddingBottom: 10,
    paddingTop: 10,
  },

  picker_style: {
    borderColor: mer_terre_blue_color,
    borderStyle: "solid",
    borderWidth: 1,
    borderRadius: 10,
    height: "auto",
    justifyContent: "center",
  },
  label_h2_form: {
    color: mer_terre_blue_color,
    fontWeight: "bold",
  },

  label_h2_input: {
    backgroundColor: form_back_color,
    borderRadius: 10,
    marginBottom: 10,
    paddingLeft: 10,
  },

  label_check_box: {
    marginLeft: 10,
  },

  checkbox_container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  checkbox_style: {
    margin: 8,
  },
  container_input_unit: {
    flexDirection: "row",
    alignSelf: "stretch",
    justifyContent: "flex-end",
  },
  input_unit: {
    position: "relative",
    right: 25,
    color: mer_terre_blue_color,
  },

  form_back_color: form_back_color,
  mer_terre_blue_color: mer_terre_blue_color,
});

const modalStyles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: "90%",
    height: "90%",
  },
  textStyle: {
    textAlign: "center",
  },
});

const homeStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
  },
});

export { s, modalStyles, homeStyles };
