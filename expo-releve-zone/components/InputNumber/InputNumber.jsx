import { Text, TextInput, View } from "react-native";
import { s } from "../../styles/styles";

export function InputNumber({inputTitle, number, onChangeNumber, unit}){
    return(
        <View style={s.container_input_unit}>
            <Text style={[s.titre_h2, s.label_h2_form, {flex:2}]}>{inputTitle} :</Text>

            <TextInput
            style={[s.titre_h2, s.label_h2_input, {width:90, color:s.mer_terre_blue_color}]}
            placeholder=" --" 
            keyboardType="numeric"
            maxLength={4}
            onChangeText={textValue => {
                if(!isNaN(textValue) && !isNaN(parseInt(textValue))) {
                    onChangeNumber(parseInt(textValue))
                } else if (textValue === "") {
                    onChangeNumber(null)
                }
            }}
            value={number?.toString()}
            />

            <Text style={[s.titre_h2, s.input_unit]}>{unit}</Text>
        </View>
    );
}
