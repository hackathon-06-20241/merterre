import { Text, TextInput, View } from "react-native";
import { useContext } from "react";
import { s } from "../../styles/styles";
import { UNITS, listeMateriaux } from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

export function InputBagVolume({
  onChangeNumber,
  inputTitle,
  number,
  material,
  unit,
  typeMeas,
}) {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  function computeTotal(fieldName, typeMeas, unit) {
    return (calcTot =
      Math.round(
        Object.keys(listeMateriaux).reduce((acc, mat) => {
          if (mat == fieldName) {
            return acc;
          } else {
            const materialName =
              unit == UNITS.liter ? "v" + typeMeas + mat : "w" + typeMeas + mat;
            return (
              acc + (releveData[materialName] ? releveData[materialName] : 0)
            );
          }
        }, 0) * 100
      ) / 100);
  }

  function updateFraction(mat, typeMeas, volTot, index) {
    const fracName = "frac" + typeMeas + mat;
    const volName = "v" + typeMeas + mat;
    setReleveData((releveData) => {
      return {
        ...releveData,
        [fracName]: Math.round((releveData[volName] / volTot) * 100),
      };
    });
  }

  function isNumeric(string) {
    return /^[+-]?\d+(\.\d+)?$/.test(string);
  }

  return (
    <View style={s.container_input_unit}>
      <Text style={[s.titre_h2, s.label_h2_form, { flex: 2 }]}>
        {inputTitle} :
      </Text>

      <TextInput
        style={[
          s.titre_h2,
          s.label_h2_input,
          { width: 110, color: s.mer_terre_blue_color },
        ]}
        inputmode="decimal"
        placeholder=" --"
        keyboardType="numeric"
        maxLength={6}
        onChangeText={(textValue) => {
          const strNum = textValue.replace(",", ".");
          const [whole, decimal] = strNum.split(".");
          const number_to_set =
            textValue == ""
              ? 0
              : decimal == ""
              ? parseFloat(whole)
              : parseFloat(strNum);

          if (isNumeric(strNum)) {
            onChangeNumber(number_to_set);
          } else {
            onChangeNumber(strNum);
          }

          const calcTot =
            computeTotal(material, typeMeas, unit) + number_to_set;
          if (unit == UNITS.liter) {
            setReleveData((releveData) => {
              return {
                ...releveData,
                ["v" + typeMeas + "TOTAL"]: calcTot,
              };
            });
          } else {
            setReleveData((releveData) => {
              return {
                ...releveData,
                ["w" + typeMeas + "TOTAL"]: calcTot,
              };
            });
          }
        }}
        value={
          number
            ? isNumeric(number)
              ? (Math.round(number * 100) / 100).toString()
              : number
            : null
        }
      />

      <Text style={[s.titre_h2, s.input_unit]}>{unit}</Text>
    </View>
  );
}
