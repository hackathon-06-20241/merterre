import { Text, TouchableOpacity } from "react-native";
import {s} from "./Button.style"

export function ButtonNav({onPress, textButton, lengthButton}){
    return (
        <TouchableOpacity onPress={onPress} style={[s.btn_nav, {width:lengthButton}]}>
            <Text style={s.txt_nav}>{textButton}</Text>
        </TouchableOpacity>
    );
}