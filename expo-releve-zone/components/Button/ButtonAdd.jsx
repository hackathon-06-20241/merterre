import { Text, TouchableOpacity } from "react-native";
import { s } from "./Button.style";

export function ButtonAdd({ onPress, textButton, lengthButton, styleSup }) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[s.btn_add, { width: lengthButton }, styleSup]}
    >
      <Text style={s.txt_add}>{textButton}</Text>
    </TouchableOpacity>
  );
}
