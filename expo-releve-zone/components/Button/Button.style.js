import { StyleSheet } from "react-native";

export const s = StyleSheet.create({
  // ButtonAdd style
  btn_add: {
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "flex-end",
    backgroundColor: "#C2DAFF",
    borderRadius: 7,
    margin: 20,
    width: 250,

    // Generateur d'ombre
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  txt_add: {
    color: "#2F76E5",
    fontWeight: "bold",
    fontSize: 18,
    paddingVertical: 6,
    //paddingHorizontal:30,
  },

  // ButtonNav style
  btn_nav: {
    flexDirection: "row",
    justifyContent: "center",
    //alignSelf:"flex-end",
    backgroundColor: "#1165af", //"#fefffb",
    borderRadius: 7,

    // Generateur d'ombre
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  txt_nav: {
    color: "white", //"#1277ba",
    fontWeight: "bold",
    fontSize: 18,
    paddingVertical: 6,
  },
});
