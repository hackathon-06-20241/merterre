import { s } from "./Container.style";
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context";

export function Container({ children }) {
  return (
    <SafeAreaProvider>
      <SafeAreaView style={s.container}>{children}</SafeAreaView>
    </SafeAreaProvider>
  );
}
