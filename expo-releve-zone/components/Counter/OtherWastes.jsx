import { s } from "./Counter.style";
import {
  Alert,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import trashIcon from "../../assets/icone_poubelle.png";
import ReleveDataContext from "../../Contexts/ReleveDataContext";
import { useContext } from "react";
import { DECHETS_AUTRES } from "../../Data/References/dechets_autres";

export function OtherWastes({ waste, deleteFunction }) {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const plusButton = (
    <TouchableOpacity style={s.btn} onPress={() => updateWaste(waste, 1)}>
      <Text style={s.txt}>{"+"}</Text>
    </TouchableOpacity>
  );

  function isNegative(n) {
    if (n < 0) {
      throw "Le nombre de déchets ne peut pas être négatif!";
    }
    return n;
  }

  const minusButton = (
    <TouchableOpacity
      style={s.btn}
      onPress={() => {
        try {
          isNegative(waste.number - 1);
          updateWaste(waste, -1);
        } catch (e) {
          Alert.alert("Oups !", e);
        }
      }}
    >
      <Text style={s.txt}>{"-"}</Text>
    </TouchableOpacity>
  );

  const deleteButton = (
    <TouchableOpacity onPress={() => deleteFunction(waste)}>
      <Image
        style={{ width: 30, height: 30, marginLeft: 10 }}
        source={trashIcon}
      />
    </TouchableOpacity>
  );

  function updateWaste(wasteToUpdate, increment) {
    const modifiedList =
      increment > 1
        ? releveData.dataDechetsAutres.map((waste) => {
            if (waste.dcsmm === wasteToUpdate.dcsmm) {
              return { ...waste, number: increment };
            }
            return waste;
          })
        : releveData.dataDechetsAutres.map((waste) => {
            if (waste.dcsmm === wasteToUpdate.dcsmm) {
              return { ...waste, number: waste.number + increment };
            }
            return waste;
          });

    setReleveData((releveData) => {
      return {
        ...releveData,
        dataDechetsAutres: modifiedList,
      };
    });
  }

  function resetWaste(wasteToUpdate) {
    const modifiedList = releveData.dataDechetsAutres.map((waste) => {
      if (waste.dcsmm === wasteToUpdate.dcsmm) {
        return { ...waste, number: null };
      }
      return waste;
    });

    setReleveData((releveData) => {
      return {
        ...releveData,
        dataDechetsAutres: modifiedList,
      };
    });
  }

  return (
    <View style={s.container}>
      <Text style={[s.txt, { flex: 1 }]}>
        {DECHETS_AUTRES.find((b) => b.dcsmm == waste.dcsmm).name}
      </Text>

      {minusButton}

      <TextInput
        style={[
          s.txt,
          { width: 50, textAlign: "center", color: s.mer_terre_blue_color },
        ]}
        placeholder="0"
        keyboardType="numeric"
        maxLength={4}
        onChangeText={(textValue) => {
          if (!isNaN(textValue) && !isNaN(parseInt(textValue))) {
            updateWaste(waste, parseInt(textValue));
          } else if (textValue === "") {
            resetWaste(waste);
          }
        }}
        value={releveData.dataDechetsAutres
          .find((b) => b.dcsmm == waste.dcsmm)
          .number?.toString()}
      />

      {plusButton}

      {deleteButton}
    </View>
  );
}
