import { useContext } from "react";
import { s } from "./Counter.style";
import { Alert, TouchableOpacity, View, Text, TextInput } from "react-native";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

export function WasteCounter({ waste }) {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const plusButton = (
    <TouchableOpacity style={s.btn} onPress={() => updateWaste(waste, 1)}>
      <Text style={s.txt}>{"+"}</Text>
    </TouchableOpacity>
  );

  function isNegative(n) {
    if (n < 0) {
      throw "Le nombre de déchets ne peut pas être négatif!";
    }
    return n;
  }

  const minusButton = (
    <TouchableOpacity
      style={s.btn}
      onPress={() => {
        try {
          isNegative(releveData.dataDechetsIndicateurs[waste.dcsmm] - 1);
          updateWaste(waste, -1);
        } catch (e) {
          Alert.alert("Oups !", e);
        }
      }}
    >
      <Text style={s.txt}>{"-"}</Text>
    </TouchableOpacity>
  );

  function updateWaste(waste, increment) {
    // Sauvegarde dans l objet releveData.dataDechetsIndicateurs
    saveReleveData(
      {
        ...releveData.dataDechetsIndicateurs,
        [waste.dcsmm]: releveData.dataDechetsIndicateurs[waste.dcsmm]
          ? releveData.dataDechetsIndicateurs[waste.dcsmm] + increment
          : increment,
      },
      "dataDechetsIndicateurs"
    );
  }

  return (
    <View style={s.container}>
      <Text style={[s.txt, { flex: 1 }]}>{waste.name}</Text>

      {minusButton}

      <TextInput
        style={[
          s.txt,
          { width: 50, textAlign: "center", color: s.mer_terre_blue_color },
        ]}
        placeholder="0"
        keyboardType="numeric"
        maxLength={4}
        onChangeText={(textValue) => {
          if (!isNaN(textValue) && !isNaN(parseInt(textValue))) {
            saveReleveData(
              {
                ...releveData.dataDechetsIndicateurs,
                [waste.dcsmm]: parseInt(textValue),
              },
              "dataDechetsIndicateurs"
            );
          } else if (textValue === "") {
            saveReleveData(
              {
                ...releveData.dataDechetsIndicateurs,
                [waste.dcsmm]: null,
              },
              "dataDechetsIndicateurs"
            );
          }
        }}
        value={releveData.dataDechetsIndicateurs[waste.dcsmm]?.toString()}
      />

      {plusButton}
    </View>
  );
}
