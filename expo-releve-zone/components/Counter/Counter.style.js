import { StyleSheet } from "react-native";

const mer_terre_blue_color = "#062f65";

const s = StyleSheet.create({
    container:{
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",

        backgroundColor:"white",
        borderRadius:13,
        paddingHorizontal:10,
        paddingVertical:10,

        // Generateur d'ombre
        shadowColor:"#000",
        shadowOffset:{
            width:0,
            height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        elevation:5,
        

    },
    btn:{
        borderColor:mer_terre_blue_color,
        borderWidth:2,
        backgroundColor:"white",
        borderRadius:15,
        width:35,
        marginLeft:5,
        marginRight:5,

    },
    txt:{
        color:mer_terre_blue_color,
        fontSize:20,
        alignSelf:"center",
    }
});

export {s};

