import { s } from "./Counter.style";
import {
  Alert,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import trashIcon from "../../assets/icone_poubelle.png";
import ReleveDataContext from "../../Contexts/ReleveDataContext";
import { useContext } from "react";

export function BrandCounter({ brand, deleteFunction }) {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const plusButton = (
    <TouchableOpacity style={s.btn} onPress={() => updateBrand(brand, 1)}>
      <Text style={s.txt}>{"+"}</Text>
    </TouchableOpacity>
  );

  function isNegative(n) {
    if (n < 0) {
      throw "Le nombre de déchets ne peut pas être négatif!";
    }
    return n;
  }

  const minusButton = (
    <TouchableOpacity
      style={s.btn}
      onPress={() => {
        try {
          isNegative(brand.number - 1);
          updateBrand(brand, -1);
        } catch (e) {
          Alert.alert("Oups !", e);
        }
      }}
    >
      <Text style={s.txt}>{"-"}</Text>
    </TouchableOpacity>
  );

  const deleteButton = (
    <TouchableOpacity onPress={() => deleteFunction(brand)}>
      <Image
        style={{ width: 30, height: 30, marginLeft: 10 }}
        source={trashIcon}
      />
    </TouchableOpacity>
  );

  function updateBrand(brandToUpdate, increment) {
    const modifiedList =
      increment > 1
        ? releveData.dataDechetsMarques.map((brand) => {
            if (brand.name === brandToUpdate.name) {
              return { ...brand, number: increment };
            }
            return brand;
          })
        : releveData.dataDechetsMarques.map((brand) => {
            if (brand.name === brandToUpdate.name) {
              return { ...brand, number: brand.number + increment };
            }
            return brand;
          });

    setReleveData((releveData) => {
      return {
        ...releveData,
        dataDechetsMarques: modifiedList,
      };
    });
  }

  function resetBrand(brandToUpdate) {
    const modifiedList = releveData.dataDechetsMarques.map((brand) => {
      if (brand.name === brandToUpdate.name) {
        return { ...brand, number: null };
      }
      return brand;
    });

    setReleveData((releveData) => {
      return {
        ...releveData,
        dataDechetsMarques: modifiedList,
      };
    });
  }

  return (
    <View style={s.container}>
      <Text style={[s.txt, { flex: 1 }]}>{brand.name}</Text>

      {minusButton}

      <TextInput
        style={[
          s.txt,
          { width: 50, textAlign: "center", color: s.mer_terre_blue_color },
        ]}
        placeholder="0"
        keyboardType="numeric"
        maxLength={4}
        onChangeText={(textValue) => {
          if (!isNaN(textValue) && !isNaN(parseInt(textValue))) {
            updateBrand(brand, parseInt(textValue));
          } else if (textValue === "") {
            resetBrand(brand);
          }
        }}
        value={releveData.dataDechetsMarques
          .find((b) => b.name == brand.name)
          .number?.toString()}
      />

      {plusButton}

      {deleteButton}
    </View>
  );
}
