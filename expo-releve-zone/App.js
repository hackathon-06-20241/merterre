import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useEffect, useState } from "react";
import { releveJson, listeMateriaux } from "./Data/releve";
import Home from "./pages/Home/Home";
import ProtocolChoice from "./pages/ProtocolChoice/ProtocolChoice";
import CountWastes from "./pages/CountWastes/CountWastes";
import VolumeBags from "./pages/VolumeBags/VolumeBags";
import WeightBags from "./pages/WeightBags/WeightBags";
import MainWastes from "./pages/MainWastes/MainWastes";
import CountBrands from "./pages/CountBrands/CountBrands";
import StockEvent from "./pages/StockEvent/StockEvent";
import GeneralInfo from "./pages/GeneralInfo/GeneralInfo";
import ReleveDataContext from "./Contexts/ReleveDataContext";
import * as FileSystem from "expo-file-system";
import * as Crypto from "expo-crypto";

import {
  NAV_HOME,
  NAV_GENERAL_INFO,
  NAV_PROTOCOL_CHOICE,
  NAV_COUNT_WASTES,
  NAV_VOLUME_BAGS,
  NAV_WEIGHT_BAGS,
  NAV_MAIN_WASTES,
  NAV_COUNT_BRANDS,
  NAV_STOCK_EVENT,
  DATA_STORAGE_PATTERN,
  DIRECTORY_URI,
  NAV_FINALIZE_EXPORT,
} from "./Data/constants";
import FinalizeExport from "./pages/FinalizeExport/FinalizeExport";

const Stack = createNativeStackNavigator();

const navTheme = {
  colors: {
    background: "transparent",
  },
};

export default function App() {
  const [releveData, setReleveData] = useState(releveJson);
  const [firstTime, setFirstTime] = useState(true);

  //console.log(releveData);

  /**
   * Fonction Générique qui permet de mettre à jour le JSON
   * @param {*} value
   * @param {*} field
   */
  function saveReleveData(value, field) {
    // A decommenter si besoin
    //console.log("value:" + value);
    //console.log("field:" + field);
    // on met à jour le champ dynamiquement après destructuration du tableau JSON
    setReleveData({ ...releveData, [field]: value });
    //console.log(releveData);
  }

  async function storeData(releveData) {
    try {
      if (releveData.pathFile === null) {
        releveData.pathFile = Crypto.randomUUID();
      }

      const dirInfo = await FileSystem.getInfoAsync(DIRECTORY_URI);
      if (!dirInfo.exists) {
        // Directory does not exist, so create it
        //console.log("Directory does not exist, creating...");
        await FileSystem.makeDirectoryAsync(DIRECTORY_URI, {
          intermediates: true,
        });
        //console.log("Directory created successfully!");
      } else {
        // Directory exists
        // console.log("Directory already exists!");
      }

      const jsonValue = JSON.stringify(releveData);
      const fileUri =
        DIRECTORY_URI + "/" + DATA_STORAGE_PATTERN + releveData.pathFile;

      //console.log("storeData fileUri:" + fileUri);
      await FileSystem.writeAsStringAsync(fileUri, jsonValue);
      // console.log("File saved successfully!");
    } catch (error) {
      console.log("Error saving the file:", error);
    }
  }

  // Persist Data
  useEffect(() => {
    if (!firstTime) {
      storeData(releveData);
    } else {
      setFirstTime(false);
    }
  }, [releveData]);

  return (
    /**
     * on passe l ensemble des données et methode pour mettre à jour le contexte
     */
    <ReleveDataContext.Provider
      value={{ releveData, setReleveData, saveReleveData, listeMateriaux }}
    >
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{ animation: "default", headerShown: false }}
          initialRouteName={NAV_HOME}
        >
          <Stack.Screen name={NAV_HOME} component={Home} />
          <Stack.Screen name={NAV_GENERAL_INFO} component={GeneralInfo} />
          <Stack.Screen name={NAV_PROTOCOL_CHOICE} component={ProtocolChoice} />
          <Stack.Screen name={NAV_COUNT_WASTES} component={CountWastes} />
          <Stack.Screen name={NAV_VOLUME_BAGS} component={VolumeBags} />
          <Stack.Screen name={NAV_WEIGHT_BAGS} component={WeightBags} />
          <Stack.Screen name={NAV_MAIN_WASTES} component={MainWastes} />
          <Stack.Screen name={NAV_COUNT_BRANDS} component={CountBrands} />
          <Stack.Screen name={NAV_STOCK_EVENT} component={StockEvent} />
          <Stack.Screen name={NAV_FINALIZE_EXPORT} component={FinalizeExport} />
        </Stack.Navigator>
      </NavigationContainer>
    </ReleveDataContext.Provider>
  );
}
