import { s } from "../../styles/styles";
import { Container } from "../../components/Container/Container";
import { Text, Image, View, ScrollView, Alert, Dimensions } from "react-native";
import { useState, useEffect, useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import { Picker } from "@react-native-picker/picker";
import { ButtonNav } from "../../components/Button/ButtonNav";

import { NAV_GENERAL_INFO, NAV_COUNT_WASTES } from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

export default function ProtocolChoice() {
  const widthScreen = Dimensions.get("window").width;

  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const nav = useNavigation();

  function goToGeneralInfo() {
    nav.navigate(NAV_GENERAL_INFO);
  }

  function goToCountWastes() {
    nav.navigate(NAV_COUNT_WASTES);
  }

  function selectProtocol(pLevel) {
    setReleveData((releveData) => {
      return {
        ...releveData,
        ["pickupLevel"]: pLevel,
      };
    });
    if (parseInt(pLevel) != 1) {
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["detailedWeight"]: true,
        };
      });
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["detailedVolume"]: true,
        };
      });
    }
  }

  function checkProtocolChoice() {
    if (
      isNaN(releveData.pickupLevel) ||
      isNaN(parseInt(releveData.pickupLevel))
    ) {
      throw "Merci de choisir un protocole de ramassage.";
    }
  }

  return (
    <Container>
      <View style={[s.page_title]}>
        <Text style={s.titre_h1}>Choix du protocole</Text>
      </View>

      <ScrollView>
        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Choisissez le protocole pour les relevés :
          </Text>

          <View style={s.picker_style}>
            <Picker
              style={s.picker_style}
              selectedValue={releveData.pickupLevel}
              onValueChange={(e) => selectProtocol(e)}
            >
              <Picker.Item
                label="Selectionner un protocole"
                value={null}
                color="#aaa"
              />
              <Picker.Item
                label="Niveau 1 - Sans Balance"
                value="1"
                color="#000"
              ></Picker.Item>
              <Picker.Item
                label="Niveau 2 - Avec Balance"
                value="2"
                color="#000"
              />
              <Picker.Item
                label="Niveau 3 - Top 34 des dechets"
                value="3"
                color="#000"
              />
              <Picker.Item
                label="Niveau 4  DCSMM (232 dechets indicateurs)"
                value="4"
                color="#000"
              />
            </Picker>
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Image
              style={{
                width: widthScreen,
                height: undefined,
                aspectRatio: 0.6,
              }}
              source={require("../../assets/Protocole_App.png")}
              resizeMode="contain"
            />
          </View>
        </View>
      </ScrollView>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 10,
        }}
      >
        <ButtonNav
          onPress={goToGeneralInfo}
          textButton={"Retour"}
          lengthButton={120}
        ></ButtonNav>
        <ButtonNav
          onPress={() => {
            try {
              checkProtocolChoice();
              goToCountWastes();
            } catch (e) {
              Alert.alert("Champ obligatoire", e);
            }
          }}
          textButton={"Continuer"}
          lengthButton={120}
        ></ButtonNav>
      </View>
    </Container>
  );
}
