import { Text, View, Alert, ScrollView } from "react-native";
import { useEffect, useState, useContext } from "react";
import { useNavigation } from "@react-navigation/native";

import { Container } from "../../components/Container/Container";
import { ButtonNav } from "../../components/Button/ButtonNav";
import { s } from "../../styles/styles";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

import XLSX from "xlsx";
import * as FileSystem from "expo-file-system";
import * as Sharing from "expo-sharing";
import * as MailComposer from "expo-mail-composer";

import { buildReleveJsonXLS } from "../../Data/releve";
import { NAV_HOME, NAV_STOCK_EVENT } from "../../Data/constants";

export default function FinalizeExport() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const [isDataDisplayed, setIsDataDisplayed] = useState(false);
  const [jsonDataReleve, setJsonDataReleve] = useState(null);

  const nav = useNavigation();

  function goToHome() {
    nav.navigate(NAV_HOME);
  }

  function gotToStockEvent() {
    nav.navigate(NAV_STOCK_EVENT);
  }

  /**
   *
   * @param {true|false} emailEnabled
   * @description Si true appelle le systeme interne d'envoie par email / Si false le document est généré est accessible sur le device
   */
  async function generateAndSendExcel(emailEnabled) {
    try {
      // Lecture rep
      console.log("lecture avant creation de fichier");
      const i = await FileSystem.readDirectoryAsync(
        FileSystem.documentDirectory
      );

      // Create workbook and worksheet
      const workbook = XLSX.utils.book_new();
      const jsonReleve = buildReleveJsonXLS(releveData);

      // Constitution du nom du fichier
      const filePath = `${FileSystem.documentDirectory}releve_${
        jsonReleve.EV_DATE_DEBUT
          ? JSON.stringify(jsonReleve.EV_DATE_DEBUT)
              .replaceAll("/", "-")
              .replaceAll('"', "")
          : "Date_Non_Définie"
      }_${
        jsonReleve.CONTEXTE_NOM_ZONE
          ? jsonReleve.CONTEXTE_NOM_ZONE
          : "Lieu_Non_Défini"
      }.xls`;

      const worksheet = XLSX.utils.json_to_sheet([jsonReleve]);
      XLSX.utils.book_append_sheet(workbook, worksheet, "imports");

      // Generate Excel file
      // Wanring => if you change bookType to xls , some cells will be cut to 256 caracters
      // Need to keep xlsx
      const wbout = XLSX.write(workbook, { type: "base64", bookType: "xlsx" });

      // Write Excel File
      FileSystem.writeAsStringAsync(filePath, wbout, { encoding: "base64" });

      // Envoi du mail
      if (emailEnabled) {
        mailOptions = {
          recipients: ["quentin.courtier@mer-terre.org"],
          subject: "Relevé Mer Terre Excel",
          attachments: [filePath],
          body: "Ci joint le relevé Excel",
          isHtml: false,
        };

        MailComposer.composeAsync(mailOptions).then((mailComposerStatus) => {
          console.log("MailComposer.composeAsync:" + mailComposerStatus.status);
          const mailStatus = "Email Status";

          switch (mailComposerStatus.status) {
            case MailComposer.MailComposerStatus.CANCELLED:
              console.log("Email annulé");
              Alert.alert(mailStatus, "L'envoi de l'email a été annulé !");
              break;
            case MailComposer.MailComposerStatus.SAVED:
              console.log("Email Sauvé");
              Alert.alert(mailStatus, "L'email a été sauvé");
              break;
            case MailComposer.MailComposerStatus.SENT:
              console.log("Email Envoyé");
              Alert.alert(mailStatus, "L'email a été envoyé");
              break;
            default:
              console.log("Cas par default");
              Alert.alert(
                mailStatus,
                "Un problème a été detecté lors de l'envoi de l'email"
              );
          }
        });
      }
      //  Stockage local
      else {
        Sharing.shareAsync(filePath);
      }
      //console.log(`File created at: ${filePath}`);
    } catch (error) {
      console.error("Error creating file:", error);
    }
  }

  async function generateExcelData() {
    Alert.alert(
      "Génerer les données",
      "Voulez vous generer les données Excel de ce relévé ?",
      [
        {
          text: "Non",
          style: "cancel",
        },
        {
          text: "Oui",
          onPress: () => {
            generateAndSendExcel(true);
          },
        },
      ]
    );
  }

  function displayViewData() {
    setJsonDataReleve(JSON.stringify(buildReleveJsonXLS(releveData), null, 2));
    setIsDataDisplayed(!isDataDisplayed);
  }

  return (
    <Container>
      <View style={[s.page_title]}>
        <Text style={s.titre_h1}>Export des données</Text>
      </View>

      <View>
        <Text style={s.titre_h2}>Vous pouver choisir de :</Text>
      </View>
      {isDataDisplayed && (
        <ScrollView style={{ height: 100 }}>
          <View>
            <Text style={s.titre_h2}>{jsonDataReleve}</Text>
          </View>
        </ScrollView>
      )}
      <View
        style={{
          height: 150,
          alignContent: "center",
          alignItems: "center",
          flexDirection: "column",
          justifyContent: "space-evenly",
        }}
      >
        <ButtonNav
          onPress={displayViewData}
          textButton={"Visualiser les données"}
          lengthButton={300}
        ></ButtonNav>
        <ButtonNav
          onPress={() => {
            try {
              generateExcelData();
            } catch (e) {
              Alert.alert("Champs obligatoires", e);
            }
          }}
          textButton={"Génerer l'export + email"}
          lengthButton={300}
        ></ButtonNav>
        <ButtonNav
          onPress={() => {
            try {
              generateAndSendExcel(false);
            } catch (e) {
              Alert.alert("Champs obligatoires", e);
            }
          }}
          textButton={"Partager l'export"}
          lengthButton={300}
        ></ButtonNav>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 10,
        }}
      >
        <ButtonNav
          onPress={gotToStockEvent}
          textButton={"Retour"}
          lengthButton={120}
        ></ButtonNav>
        <ButtonNav
          onPress={goToHome}
          textButton={"Accueil"}
          lengthButton={120}
        ></ButtonNav>
      </View>
    </Container>
  );
}
