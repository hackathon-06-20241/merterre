import { s } from "../../styles/styles";
import { Container } from "../../components/Container/Container";
import {
  Text,
  View,
  ScrollView,
  Switch,
  TextInput,
  Alert,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useState, useEffect, useContext } from "react";
import { Checkbox } from "expo-checkbox";
import { InputNumber } from "../../components/InputNumber/InputNumber";
import { InputBagVolume } from "../../components/InputNumber/InputBagVolume";
import { ButtonNav } from "../../components/Button/ButtonNav";

import { NAV_WEIGHT_BAGS, NAV_COUNT_BRANDS } from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

import {
  UNITS,
  DEFAULT_UNIT,
  getOppositeUnit,
  listeMateriaux,
} from "../../Data/constants";

let isFirstRender = true;

export default function VolumeMainWastes() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const [currentUnit, setCurrentUnit] = useState(DEFAULT_UNIT);
  const toggleSwitch = () => setCurrentUnit(getOppositeUnit(currentUnit));

  const nav = useNavigation();

  function goToCountBrands() {
    nav.navigate(NAV_COUNT_BRANDS);
  }

  function goToWeightBags() {
    nav.navigate(NAV_WEIGHT_BAGS);
  }

  function isNumeric(string) {
    return /^[+-]?\d+(\.\d+)?$/.test(string);
  }
  function updateVolume(mat, volTot, index) {
    const fracName = "fracMain_" + mat;
    const volName = "vMain_" + mat;
    const newVol = Math.round(releveData[fracName] * volTot) / 100;
    setReleveData((releveData) => {
      return {
        ...releveData,
        [volName]: newVol,
      };
    });
  }

  function updateFraction(mat, volTot, index) {
    const fracName = "fracMain_" + mat;
    const volName = "vMain_" + mat;
    const newFrac =
      Math.round((releveData[volName] / volTot) * 100 * 100000) / 100000;
    setReleveData((releveData) => {
      return {
        ...releveData,
        [fracName]: newFrac,
      };
    });
  }

  function resetField(mat, typeField, index) {
    const fieldname = typeField + "Main_" + mat;
    setReleveData((releveData) => {
      return {
        ...releveData,
        [fieldname]: null,
      };
    });
  }

  function computeTotalVolume() {
    return (calcVolTot =
      Math.round(
        Object.keys(listeMateriaux).reduce((acc, mat) => {
          const materialName = "vMain_" + mat;
          return (
            acc + (releveData[materialName] ? releveData[materialName] : 0)
          );
        }, 0) * 100
      ) / 100);
  }

  function switchToDetailed() {
    if (!isFirstRender && releveData.detailedVolume) {
      if (releveData.vMain_TOTAL > 0) {
        Alert.alert(
          "Attention",
          "La saisie des volumes détaillés sera perdue.",
          [
            {
              text: "Annuler",
              style: "cancel",
            },
            {
              text: "Continuer",
              style: "destructive",
              onPress: () => {
                saveReleveData(false, "detailedVolume");
                [...Object.keys(listeMateriaux), "TOTAL"].map((key, index) =>
                  resetField(key, "v", index)
                );
                [...Object.keys(listeMateriaux), "TOTAL"].map((key, index) =>
                  resetField(key, "frac", index)
                );
              },
            },
          ]
        );
      } else {
        saveReleveData(false, "detailedVolume");
      }
    } else {
      saveReleveData(true, "detailedVolume");
    }
  }

  function inputMeasure(mat, currentUnit, typeMeas, index) {
    const fieldType = typeMeas + mat;
    const fieldName =
      currentUnit == UNITS.liter ? "v" + fieldType : "frac" + fieldType;
    return (
      <InputBagVolume
        key={index}
        onChangeNumber={(e) => {
          saveReleveData(e, fieldName);
        }}
        number={releveData[fieldName]}
        material={mat}
        inputTitle={listeMateriaux[mat]}
        unit={currentUnit}
        typeMeas={typeMeas}
      ></InputBagVolume>
    );
  }

  function setTotal(isvolume) {
    const fieldTotal = isvolume ? "vMain_TOTAL" : "wMain_TOTAL";
    const condition = isvolume
      ? releveData.detailedVolume && currentUnit != UNITS.percentage
      : false;
    const unit = isvolume ? "L" : "kg";
    return (
      <View
        style={[
          s.container_input_unit,
          {
            flex: 1,
            justifyContent: "left",
            marginLeft: 10,
          },
        ]}
      >
        <TextInput
          style={
            condition
              ? [
                  s.titre_h2,
                  s.label_h2_input,
                  {
                    width: 130,
                    color: s.mer_terre_blue_color,
                    backgroundColor: "transparent",
                  },
                ]
              : [s.titre_h2, s.label_h2_input, { width: 130, marginRight: -5 }]
          }
          inputmode="decimal"
          placeholder="--"
          keyboardType="numeric"
          maxLength={8}
          editable={condition ? false : true}
          onChangeText={(textValue) => {
            const strNum = textValue.replace(",", ".");
            const [whole, decimal] = strNum.split(".");
            const number_to_set =
              textValue == ""
                ? 0
                : decimal == ""
                ? parseFloat(whole)
                : parseFloat(strNum);

            if (isNumeric(strNum)) {
              saveReleveData(number_to_set, fieldTotal);
            } else {
              saveReleveData(strNum, fieldTotal);
            }
          }}
          value={
            isvolume
              ? releveData.vMain_TOTAL
                ? isNumeric(releveData.vMain_TOTAL)
                  ? (Math.round(releveData.vMain_TOTAL * 100) / 100).toString()
                  : releveData.vMain_TOTAL
                : null
              : releveData.wMain_TOTAL
              ? isNumeric(releveData.wMain_TOTAL)
                ? (Math.round(releveData.wMain_TOTAL * 100) / 100).toString()
                : releveData.wMain_TOTAL
              : null
          }
        />
        <Text style={[s.titre_h2, s.input_unit]}>{unit}</Text>
      </View>
    );
  }

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    } else {
      if (releveData.vMain_TOTAL != 0) {
        if (currentUnit != UNITS.liter) {
          Object.keys(listeMateriaux).map((key, index) =>
            updateVolume(key, releveData.vMain_TOTAL, index)
          );
        }
      }
    }
  }, [
    releveData.vMain_TOTAL,
    releveData.fracMain_Plastique,
    releveData.fracMain_Caoutchouc,
    releveData.fracMain_Bois,
    releveData.fracMain_Textile,
    releveData.fracMain_Carton,
    releveData.fracMain_Metal,
    releveData.fracMain_Verre,
    releveData.fracMain_Unknown,
  ]);

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    } else {
      const calcVolTot = computeTotalVolume();
      if (currentUnit == UNITS.percentage && calcVolTot > 0) {
        Object.keys(listeMateriaux).map((key, index) =>
          updateFraction(key, calcVolTot, index)
        );
      }
    }
  }, [currentUnit, releveData.detailedVolume]);

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    }
  }, [
    releveData.vMain_Plastique,
    releveData.vMain_Caoutchouc,
    releveData.vMain_Bois,
    releveData.vMain_Textile,
    releveData.vMain_Carton,
    releveData.vMain_Metal,
    releveData.vMain_Verre,
    releveData.vMain_Unknown,
  ]);

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    } else {
      if (releveData.detailedVolume) {
        const calcVolTot = computeTotalVolume();
        setReleveData((releveData) => {
          return {
            ...releveData,
            ["vMain_TOTAL"]: calcVolTot,
          };
        });
      }
    }
  }, [releveData.detailedVolume]);

  function checkPercentagesConsistency(isConversion) {
    if (currentUnit == UNITS.percentage) {
      const delta = 0.1; // pour gérer les arrondis

      const sum_percentages = Object.keys(listeMateriaux).reduce((acc, mat) => {
        const materialName = "fracMain_" + mat;
        return acc + (releveData[materialName] ? releveData[materialName] : 0);
      }, 0);

      if (releveData.vMain_TOTAL != null && releveData.vMain_TOTAL != 0) {
        if (sum_percentages + delta < 100 || sum_percentages - delta > 100) {
          if (isConversion) {
            throw "Pour passer d'une saisie en % à une saisie en L, la somme des pourcentages des matériaux doit être égale à 100%. Ajustez les pourcentages ou supprimez le volume total pour revenir à la saisie en L.";
          } else {
            throw "Si le volume total des déchets volumineux est non nul, indiquez la répartition. La somme des pourcentages des matériaux doit être égale à 100%.";
          }
        }
      } else {
        if (sum_percentages != 0) {
          throw "Vous n'avez pas indiqué le volume total.";
        }
      }
    }
  }

  return (
    <Container>
      <View style={s.page_title}>
        <Text style={s.titre_h1}>Déchets volumineux</Text>
      </View>

      <ScrollView>
        <View style={s.txt_normal}>
          <Text>
            Les déchets volumineux correspondent à tous les déchets qui ne
            rentrent pas dans les sacs. Vous pouvez estimer un volume en Litres
            pour chaque matériau ou, s’ils sont nombreux, saisir un volume total
            avec ou sans pourcentage par matériau. Merci de renseigner dans les
            commentaires quels types de déchets volumineux sont présents.
          </Text>
        </View>

        <Text style={[s.titre_h2, s.label_h2_form]}>
          Volume total des déchets volumineux :
        </Text>

        <View style={{ flexDirection: "row", alignItems: "center" }}>
          {setTotal(true)}
          <View style={{ flex: 1 }}>
            {!!releveData.detailedVolume && (
              <View
                style={[
                  s.txt_normal,
                  { flexDirection: "column", alignItems: "center" },
                ]}
              >
                <Switch
                  //trackColor={{false: '#767577', true: '#81b0ff'}}
                  //thumbColor={isPercent ? '#f5dd4b' : '#f4f3f4'}
                  //ios_backgroundColor="#3e3e3e"
                  onValueChange={() => {
                    try {
                      checkPercentagesConsistency(true);
                      toggleSwitch();
                    } catch (e) {
                      Alert.alert("Attention !", e);
                    }
                  }}
                  value={currentUnit}
                />
                {currentUnit == UNITS.percentage ? (
                  <>
                    <Text>Saisie en pourcentage</Text>
                  </>
                ) : (
                  <>
                    <Text>Saisie en litres</Text>
                  </>
                )}
              </View>
            )}
          </View>
        </View>

        {releveData.pickupLevel == 1 ? (
          <View>
            <TouchableOpacity
              style={s.checkbox_container}
              onPress={() => {
                switchToDetailed();
              }}
            >
              <Checkbox
                color={s.mer_terre_blue_color}
                style={s.checkbox_style}
                value={releveData.detailedVolume}
              />
              <Text
                style={[
                  s.titre_h2,
                  s.label_check_box,
                  { flex: 1, flexWrap: "wrap" },
                ]}
              >
                Saisir la répartition du volume hors sacs par matériau
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <Text style={[s.titre_h2, s.label_h2_form, { marginTop: 15 }]}>
            Répartition du volume par matériau :
          </Text>
        )}

        {!!releveData.detailedVolume && (
          <View style={{ marginLeft: 10 }}>
            {Object.keys(listeMateriaux).map((key, index) =>
              inputMeasure(key, currentUnit, "Main_", index)
            )}
          </View>
        )}

        <Text style={[s.titre_h2, s.label_h2_form]}>
          Poids total des déchets volumineux :
        </Text>

        <View style={{ flexDirection: "row", alignItems: "center" }}>
          {setTotal(false)}
        </View>

        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>Commentaires :</Text>
          <TextInput
            style={[s.titre_h2, s.label_h2_input, { marginHorizontal: 10 }]}
            value={releveData.commentaire}
            onChangeText={(e) => {
              saveReleveData(e, "DV_comments");
            }}
            placeholder="Précisez ici les types de déchets volumineux ramassés."
            multiline={true}
            numberOfLines={2}
          />
        </View>
      </ScrollView>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 10,
        }}
      >
        <ButtonNav
          onPress={goToWeightBags}
          textButton={"Retour"}
          lengthButton={120}
        ></ButtonNav>
        <ButtonNav
          onPress={() => {
            try {
              checkPercentagesConsistency(false);
              goToCountBrands();
            } catch (e) {
              Alert.alert("Attention !", e);
            }
          }}
          textButton={"Continuer"}
          lengthButton={120}
        ></ButtonNav>
      </View>
    </Container>
  );
}
