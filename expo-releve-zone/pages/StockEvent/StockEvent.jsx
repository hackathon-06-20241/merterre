import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Alert,
  Switch,
} from "react-native";
import { ButtonNav } from "../../components/Button/ButtonNav";
import { Container } from "../../components/Container/Container";
import { useNavigation } from "@react-navigation/native";
import { s } from "../../styles/styles";
import {
  requestForegroundPermissionsAsync,
  getCurrentPositionAsync,
} from "expo-location";

import {
  checkIsFieldEmpty,
  NAV_COUNT_BRANDS,
  NAV_HOME,
  NAV_FINALIZE_EXPORT,
} from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

import MapView, { Polygon, Marker, Polyline } from "react-native-maps"; // remove PROVIDER_GOOGLE import if not using Google Maps
import { useEffect, useState, useContext } from "react";

import { roundToDecimalPlaces, useThousandSeparator } from "../../Data/releve";
import {
  TAB_LIEUX_MILIEUX,
  TYPE_LIEU,
  TYPE_MILIEU,
} from "../../Data/References/types-lieux";

const styles = StyleSheet.create({
  map_container: {
    height: 300,
    width: "auto",
    justifyContent: "center",
    alignItems: "center",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default function StockEvent() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const nav = useNavigation();

  // Manage Switch mode of the map
  const [isPolygonMode, setIsPolygonMode] = useState(true);
  const toggleSwitch = () =>
    setIsPolygonMode((previousState) => !previousState);

  const [typeLieuMilieu, setTypeLieuMilieu] = useState(null);

  function checkFields() {
    if (
      checkIsFieldEmpty(releveData.zoneName) ||
      checkIsFieldEmpty(releveData.dureeH) ||
      checkIsFieldEmpty(releveData.surfaceTotale)
    ) {
      throw "Veuillez compléter les champs marqués d'une astérisque pour continuer.";
    }

    if (
      typeLieuMilieu?.linear_data &&
      checkIsFieldEmpty(releveData.linearDistance)
    ) {
      throw "Vous devez saisir au moins deux points pour la saisie du linéaire !";
    }
  }

  function goToCountBrands() {
    nav.navigate(NAV_COUNT_BRANDS);
  }

  function goToFinalizeExport() {
    try {
      checkFields();
      nav.navigate(NAV_FINALIZE_EXPORT);
    } catch (e) {
      Alert.alert("Champs obligatoires", e);
    }
  }

  const handleMapPress = (event) => {
    let { coordinate } = event.nativeEvent;

    const latitude = roundToDecimalPlaces(coordinate.latitude, 6);
    const longitude = roundToDecimalPlaces(coordinate.longitude, 6);

    coordinate = { latitude, longitude };

    setReleveData((releveData) => {
      if (isPolygonMode) {
        return {
          ...releveData,
          ["polygonCoords"]: [...releveData.polygonCoords, coordinate],
        };
      } else {
        return {
          ...releveData,
          ["linearCoords"]: [...releveData.linearCoords, coordinate],
        };
      }
    });
  };

  useEffect(() => {
    const selectedLieuMilieu = TAB_LIEUX_MILIEUX.filter(
      (group) =>
        group.id_milieu == releveData?.typeMilieu &&
        group.id_lieu == releveData?.typeLieu
    )[0];
    setTypeLieuMilieu(selectedLieuMilieu);
  }, []);

  // Mise à surface total
  useEffect(() => {
    if (releveData.polygonCoords.length >= 3) {
      //console.log("Calcul de la surface");
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["surfaceTotale"]: calculateSurfaceArea(releveData.polygonCoords),
        };
      });
    } else {
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["surfaceTotale"]: null,
        };
      });
    }
    setReleveData((releveData) => {
      return {
        ...releveData,
        ["centroid"]: calculateCentroid(releveData.polygonCoords),
      };
    });
  }, [releveData.polygonCoords]);

  // Mise à jour distance lineaire
  useEffect(() => {
    if (releveData.linearCoords.length >= 2) {
      //console.log("Calcul de la surface");
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["linearDistance"]: calculateLinearDistance(releveData.linearCoords),
        };
      });
    } else {
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["linearDistance"]: null,
        };
      });
    }
  }, [releveData.linearCoords]);

  useEffect(() => {
    getUserCoords();
  }, []);

  async function getUserCoords() {
    //console.log("request coordonnees");
    let { status } = await requestForegroundPermissionsAsync();
    if (status === "granted") {
      const location = await getCurrentPositionAsync();
      //console.log("Coordonnees recuperees:");
      //console.log(location);
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["userLat"]: location.coords.latitude,
        };
      });
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["userLon"]: location.coords.longitude,
        };
      });
    } else {
      setCoords({ lat: "48.85", lng: "2.35" });
    }
  }

  const calculateSurfaceArea = (coordinates) => {
    if (coordinates.length < 3) return 0;

    let area = 0;
    for (let i = 0; i < coordinates.length; i++) {
      const j = (i + 1) % coordinates.length;
      area += coordinates[i].latitude * coordinates[j].longitude;
      area -= coordinates[j].latitude * coordinates[i].longitude;
    }
    area = Math.abs(area) / 2;

    // Convert to square kilometers (assuming coordinates are in degrees)
    const earthRadius = 6371; // km
    const surfaceArea =
      area * (Math.PI / 180) * (Math.PI / 180) * earthRadius * earthRadius;
    //("surfaceArea:" + surfaceArea);
    // * 1000 pour mettre carré
    return (surfaceArea * 1000000).toFixed(0);
  };

  // Haversine formula to calculate distance between two points
  function calculateDistance(lat1, lon1, lat2, lon2) {
    const R = 6371e3; // Earth's radius in meters
    const φ1 = (lat1 * Math.PI) / 180;
    const φ2 = (lat2 * Math.PI) / 180;
    const Δφ = ((lat2 - lat1) * Math.PI) / 180;
    const Δλ = ((lon2 - lon1) * Math.PI) / 180;

    const a =
      Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return R * c; // Distance in meters
  }

  const calculateLinearDistance = (coordinates) => {
    let totalDistance = 0;
    for (let i = 0; i < coordinates.length - 1; i++) {
      const { latitude: lat1, longitude: lon1 } = coordinates[i];
      const { latitude: lat2, longitude: lon2 } = coordinates[i + 1];
      totalDistance += calculateDistance(lat1, lon1, lat2, lon2);
    }
    return totalDistance.toFixed(0);
  };

  function toCoordCart_X(p) {
    return (
      Math.cos((p.latitude * Math.PI) / 180) *
      Math.cos((p.longitude * Math.PI) / 180)
    );
  }

  function toCoordCart_Y(p) {
    return (
      Math.cos((p.latitude * Math.PI) / 180) *
      Math.sin((p.longitude * Math.PI) / 180)
    );
  }

  function toCoordCart_Z(p) {
    return Math.sin((p.latitude * Math.PI) / 180);
  }

  const calculateCentroid = (coordinates) => {
    const n_points = coordinates.length;
    if (n_points < 3) {
      return {};
    }

    // Calcul des coordonnées du centroide en coordonnées cartésiennes
    let x_c = 0;
    let y_c = 0;
    let z_c = 0;
    for (let i = 0; i < n_points; i++) {
      x_c += toCoordCart_X(coordinates[i]);
      y_c += toCoordCart_Y(coordinates[i]);
      z_c += toCoordCart_Z(coordinates[i]);
    }
    x_c = x_c / n_points;
    y_c = y_c / n_points;
    z_c = z_c / n_points;

    // Conversion en coordonnées GPS
    const latitude =
      Math.round(
        ((Math.atan2(z_c, Math.sqrt(x_c * x_c + y_c * y_c)) * 180) / Math.PI) *
          100000
      ) / 100000;
    const longitude =
      Math.round(((Math.atan2(y_c, x_c) * 180) / Math.PI) * 100000) / 100000;

    centroid = { latitude, longitude };
    return centroid;
  };

  function removeLastMarker() {
    if (isPolygonMode) {
      if (releveData.polygonCoords?.length > 0) {
        setReleveData((releveData) => {
          return {
            ...releveData,
            ["polygonCoords"]: releveData.polygonCoords.slice(0, -1),
          };
        });
      }
    } else {
      if (releveData.linearCoords?.length > 0) {
        setReleveData((releveData) => {
          return {
            ...releveData,
            ["linearCoords"]: releveData.linearCoords.slice(0, -1),
          };
        });
      }
    }
  }

  return (
    <Container>
      <View style={s.page_title}>
        <Text style={s.titre_h1}>Couverture de l'événement</Text>
      </View>
      <ScrollView style={{ flex: 1 }}>
        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Dessin de la zone de ramassage (*)
          </Text>
        </View>
        <View>
          <Text style={s.titre_h3}>
            {typeLieuMilieu?.linear_data
              ? "Veuillez indiquer la surface et le linéaire du ramassage "
              : "Veuillez indiquer la surface du ramassage "}
            pour la zone de type {TYPE_MILIEU[typeLieuMilieu?.id_milieu]} /{" "}
            {TYPE_LIEU[typeLieuMilieu?.id_lieu]} :
          </Text>
        </View>
        {typeLieuMilieu?.linear_data && (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              alignContent: "center",
              marginBottom: -10,
            }}
          >
            <Text
              style={[s.titre_h2, s.label_h2_form, { alignSelf: "center" }]}
            >
              {isPolygonMode ? "Saisie de la surface" : "Saisie du linéaire"}
            </Text>
            <Switch
              style={[s.titre_h2, s.label_h2_form, { alignSelf: "center" }]}
              onValueChange={toggleSwitch}
              value={isPolygonMode}
              thumbColor={isPolygonMode ? "red" : "#69d206"}
              trackColor={{
                false: s.form_back_color,
                true: s.form_back_color,
              }}
            />
          </View>
        )}
        <View>
          <Text style={{ paddingBottom: 15 }}>
            {isPolygonMode
              ? "Positionnez sur la carte les coins du polygone schématisant la zone ramassée"
              : "Positionnez sur la carte les extremités du linéaire correspondant à la longueur de plage/rivière/route ramassée"}
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            paddingBottom: 15,
          }}
        >
          <ButtonNav
            onPress={removeLastMarker}
            textButton={"Annuler le dernier point saisi"}
            lengthButton={310}
          ></ButtonNav>
        </View>

        <View style={{ paddingBottom: 10 }}>
          <View>
            <Text>
              Surface totale de:
              {releveData.surfaceTotale
                ? " " +
                  useThousandSeparator(releveData.surfaceTotale) +
                  " m2 (" +
                  (releveData.surfaceTotale / 10000).toFixed(1) +
                  " ha)"
                : null}
            </Text>
          </View>
          {typeLieuMilieu?.linear_data && (
            <View>
              <Text>
                Distance linéaire totale de:
                {releveData.linearDistance
                  ? " " + useThousandSeparator(releveData.linearDistance) + " m"
                  : null}
              </Text>
            </View>
          )}
        </View>

        {/* La map */}
        <View style={styles.map_container}>
          <MapView
            showsUserLocation={true} // Apple maps only
            showsScale={true} // Display scale only in ios
            userLocationCalloutEnabled={true} // Apple maps only
            style={styles.map}
            mapType="hybrid"
            region={{
              // pb sur IOS, si les données du user ne sont pas présentes
              // on affiche alors les données de paris (sinon exception)
              latitude: releveData.userLat ? releveData.userLat : 48.86,
              longitude: releveData.userLon ? releveData.userLon : 2.35,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}
            onPress={handleMapPress}
          >
            {releveData.polygonCoords.length >= 3 && (
              <Polygon
                coordinates={releveData.polygonCoords}
                fillColor="rgba(0, 200, 0, 0.5)"
                strokeColor="rgba(0, 0, 0, 0.5)"
                strokeWidth={1}
              />
            )}
            {releveData.linearCoords.length >= 2 && (
              <Polyline
                coordinates={releveData.linearCoords}
                strokeColor="rgba(0, 0, 0, 0.5)"
                strokeWidth={2}
                geodesic={true}
              />
            )}
            {releveData.polygonCoords.map((coord, index) => (
              <Marker key={index} coordinate={coord} pinColor="red" />
            ))}
            {releveData.linearCoords.map((coord, index) => (
              <Marker key={index} coordinate={coord} pinColor="green" />
            ))}
            {Object.keys(releveData.centroid).length != 0 && (
              <Marker coordinate={releveData.centroid} pinColor="orange" />
            )}
          </MapView>
        </View>

        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Nom de la zone (*) :
          </Text>
          <TextInput
            style={[s.titre_h2, s.label_h2_input]}
            value={releveData.zoneName}
            onChangeText={(e) => {
              saveReleveData(e, "zoneName");
            }}
            placeholder="Nom de la zone"
          />
        </View>

        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Durée en heures (*) :
          </Text>
          <TextInput
            style={[s.titre_h2, s.label_h2_input]}
            value={releveData.dureeH}
            onChangeText={(e) => {
              saveReleveData(e, "dureeH");
            }}
            placeholder="Durée en heures"
            keyboardType="decimal-pad"
          />
        </View>

        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>Commentaires :</Text>
          <TextInput
            style={[s.titre_h2, s.label_h2_input]}
            value={releveData.commentaire}
            onChangeText={(e) => {
              saveReleveData(e, "commentaire");
            }}
            placeholder="Commentaires"
            multiline={true}
            numberOfLines={4}
          />
        </View>
      </ScrollView>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 10,
        }}
      >
        <ButtonNav
          onPress={goToCountBrands}
          textButton={"Retour"}
          lengthButton={120}
        ></ButtonNav>
        <ButtonNav
          onPress={goToFinalizeExport}
          textButton={"Continuer"}
          lengthButton={120}
        ></ButtonNav>
      </View>
    </Container>
  );
}
