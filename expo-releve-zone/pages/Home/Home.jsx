import { StatusBar } from "expo-status-bar";
import {
  FlatList,
  ImageBackground,
  Modal,
  TouchableOpacity,
} from "react-native";
import { Alert, Image, StyleSheet, Text, View, Dimensions } from "react-native";
import { Container } from "../../components/Container/Container";
import { useNavigation } from "@react-navigation/native";
import { ButtonNav } from "../../components/Button/ButtonNav";
import ReleveDataContext from "../../Contexts/ReleveDataContext";
import { useContext, useEffect, useState } from "react";
import { releveJson } from "../../Data/releve";
import appJson from "../../app.json";
import { s, modalStyles, homeStyles } from "../../styles/styles";

import {
  DATA_STORAGE_PATTERN,
  NAV_GENERAL_INFO,
  DIRECTORY_URI,
} from "../../Data/constants";
import { SafeAreaView } from "react-native-safe-area-context";
import * as FileSystem from "expo-file-system";

export default function Home() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);
  const [modalVisible, setModalVisible] = useState(false);

  const [listDataStore, setListDataStore] = useState(null);

  const nav = useNavigation();

  function gotoNewData() {
    if (releveData.releveInProgress) {
      Alert.alert(
        "Nouveau Relevé",
        "Voulez vous commencer un nouveau relevé ?",
        [
          {
            text: "Non",
            style: "cancel",
          },
          {
            text: "Oui",
            onPress: () => {
              // Reset Data
              setReleveData(releveJson);
              nav.navigate(NAV_GENERAL_INFO);
            },
          },
        ]
      );
    } else {
      setReleveData(releveJson);
      nav.navigate(NAV_GENERAL_INFO);
    }
  }

  function gotoPreviousData() {
    nav.navigate(NAV_GENERAL_INFO);
  }

  async function launchModalStorage() {
    await getStorageData().then((arrayJsonReleve) => {
      setListDataStore(arrayJsonReleve);
      setModalVisible(true);
    });
  }

  function onCloseModal() {
    setModalVisible(false);
  }

  function onDeleteItem(item) {
    Alert.alert(
      "Suppression fichier",
      "Voulez-vous supprimer le relevé selectionné ?",
      [
        {
          text: "Non",
          style: "cancel",
        },
        {
          text: "Oui",
          onPress: () => {
            // Reset Data
            deleteReleveDataFile(item);
            //nav.navigate(NAV_GENERAL_INFO);
          },
        },
      ]
    );
  }

  /**
   *
   * @param {*} item
   */
  async function deleteReleveDataFile(releveData) {
    const fileUri =
      DIRECTORY_URI + "/" + DATA_STORAGE_PATTERN + releveData.pathFile;
    try {
      // Check if the file exists before trying to delete
      const fileInfo = await FileSystem.getInfoAsync(fileUri);

      if (fileInfo.exists) {
        // Delete the file if it exists
        await FileSystem.deleteAsync(fileUri);
        //console.log("Fichier supprimé");
        //setMessage(`${fileName} has been deleted successfully.`);
        // Update UI
        const updatedItems = listDataStore.filter(
          (item) => item.pathFile !== releveData.pathFile
        );
        setListDataStore(updatedItems);
      } else {
        console.log("Le fichier n'existe pas !");
        //setMessage(`${fileName} does not exist.`);
      }
    } catch (error) {
      console.error("Error deleting the file:", error);
      //setMessage(`Error deleting ${fileName}.`);
    }
  }

  /**
   * Get all the logic to parse DATA_STORAGE_DIR and get the content of each files
   * @returns a json table of files content
   */
  async function getStorageData() {
    try {
      //console.log(directoryUri);
      // Get the list of files in the directory
      const stringArrayFileName = await FileSystem.readDirectoryAsync(
        DIRECTORY_URI
      );
      //console.log("Files:", stringArrayFileName);
      let tabJson = [];
      for (let index = 0; index < stringArrayFileName.length; index++) {
        const fileName = stringArrayFileName[index];
        const filePath = DIRECTORY_URI + "/" + fileName;
        const jsonString = await FileSystem.readAsStringAsync(filePath);
        const jsonData = JSON.parse(jsonString);
        tabJson.push(jsonData);
      }
      return tabJson;
    } catch (e) {
      console.error(e);
    }
  }

  function onSelectItem(item) {
    setReleveData(item);
    setModalVisible(false);
    nav.navigate(NAV_GENERAL_INFO);
  }

  return (
    <Container>
      <View style={homeStyles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Image
            style={{ flex: 1 }}
            source={require("../../assets/zero_dechet_logo.png")}
            resizeMode="contain"
          />

          <Image
            style={{ flex: 1 }}
            source={require("../../assets/merterre_background.jpg")}
            resizeMode="cover"
          />
        </View>
        <View
          style={{
            flex: 0.3,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text style={{ fontSize: 25, textAlign: "center" }}>
            Application de saisie des données de caractérisation des déchets
          </Text>
        </View>
        <View
          style={{
            flex: 0.3,
            flexDirection: "column",
            justifyContent: "space-evenly",
          }}
        >
          {/* <ButtonNav onPress={goToCreateEvt} textButton={"Créer un évènement"} lengthButton={220}></ButtonNav> */}
          <ButtonNav
            onPress={gotoNewData}
            textButton={"Commencer un nouveau relevé"}
            lengthButton={310}
          ></ButtonNav>
          <ButtonNav
            onPress={launchModalStorage}
            textButton={"Accéder aux anciens relevés"}
            lengthButton={310}
          ></ButtonNav>
          {releveData.releveInProgress && (
            <ButtonNav
              onPress={gotoPreviousData}
              textButton={"Acceder au relevé en cours"}
              lengthButton={310}
            ></ButtonNav>
          )}
        </View>
        <View>
          <Text style={{ fontSize: 15, textAlign: "center" }}>
            Version {appJson.expo.version}
          </Text>
        </View>
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          onRequestClose={onCloseModal}
        >
          <SafeAreaView style={modalStyles.centeredView}>
            <View style={modalStyles.modalView}>
              <Text style={[s.titre_h2, s.label_h2_form]}>
                Liste des relevés disponibles:
              </Text>
              <Text style={{ fontSize: 15, textAlign: "center" }}>
                (appui long pour supprimer)
              </Text>
              <FlatList
                data={listDataStore}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    style={modalStyles.option}
                    onLongPress={() => {
                      onDeleteItem(item);
                    }}
                    onPress={() => {
                      onSelectItem(item);
                    }}
                  >
                    <Text style={[s.titre_h2, modalStyles.textStyle]}>
                      {item.evtName} - {item.evtDate}
                    </Text>
                  </TouchableOpacity>
                )}
                //keyExtractor={(item) => item.id.toString()}
              />
              <TouchableOpacity style={modalStyles.closeButton}>
                <ButtonNav
                  textButton={"Fermer"}
                  lengthButton={110}
                  onPress={onCloseModal}
                ></ButtonNav>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </Modal>
      </View>
    </Container>
  );
}
