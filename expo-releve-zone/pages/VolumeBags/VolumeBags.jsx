import { s } from "../../styles/styles";
import { Container } from "../../components/Container/Container";
import { Text, View, ScrollView, Switch, TextInput, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useState, useEffect, useContext } from "react";
import { ButtonNav } from "../../components/Button/ButtonNav";
import { InputNumber } from "../../components/InputNumber/InputNumber";
import { InputBagVolume } from "../../components/InputNumber/InputBagVolume";

import { NAV_COUNT_WASTES, NAV_WEIGHT_BAGS } from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

import {
  UNITS,
  DEFAULT_UNIT,
  getOppositeUnit,
  listeMateriaux,
} from "../../Data/constants";

let isFirstRender = true;

export default function VolumeBags() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const [currentUnit, setCurrentUnit] = useState(DEFAULT_UNIT);
  const toggleSwitch = () => setCurrentUnit(getOppositeUnit(currentUnit));

  const nav = useNavigation();

  function goToCountWastes() {
    nav.navigate(NAV_COUNT_WASTES);
  }

  function goToWeightBags() {
    nav.navigate(NAV_WEIGHT_BAGS);
  }

  function updateVolume(mat, volTot, index) {
    const fracName = "fracBags_" + mat;
    const volName = "vBags_" + mat;
    const newVol = Math.round(releveData[fracName] * volTot) / 100;
    setReleveData((releveData) => {
      return {
        ...releveData,
        [volName]: newVol,
      };
    });
  }

  function updateFraction(mat, volTot, index) {
    const fracName = "fracBags_" + mat;
    const volName = "vBags_" + mat;
    const newFrac =
      Math.round((releveData[volName] / volTot) * 100 * 100000) / 100000;
    setReleveData((releveData) => {
      return {
        ...releveData,
        [fracName]: newFrac,
      };
    });
  }

  function computeTotalVolume() {
    return (calcVolTot =
      Math.round(
        Object.keys(listeMateriaux).reduce((acc, mat) => {
          const materialName = "vBags_" + mat;
          return (
            acc + (releveData[materialName] ? releveData[materialName] : 0)
          );
        }, 0) * 100
      ) / 100);
  }

  function inputMeasure(mat, currentUnit, typeMeas, index) {
    const fieldType = typeMeas + mat;
    const fieldName =
      currentUnit == UNITS.liter ? "v" + fieldType : "frac" + fieldType;
    return (
      <InputBagVolume
        key={index}
        onChangeNumber={(e) => {
          saveReleveData(e, fieldName);
        }}
        number={releveData[fieldName]}
        material={mat}
        inputTitle={listeMateriaux[mat]}
        unit={currentUnit}
        typeMeas={typeMeas}
      ></InputBagVolume>
    );
  }

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    } else {
      if (releveData.vBags_TOTAL != 0) {
        if (currentUnit != UNITS.liter) {
          Object.keys(listeMateriaux).map((key, index) =>
            updateVolume(key, releveData.vBags_TOTAL, index)
          );
        }
      }
    }
  }, [
    releveData.vBags_TOTAL,
    releveData.fracBags_Plastique,
    releveData.fracBags_Caoutchouc,
    releveData.fracBags_Bois,
    releveData.fracBags_Textile,
    releveData.fracBags_Carton,
    releveData.fracBags_Metal,
    releveData.fracBags_Verre,
    releveData.fracBags_Unknown,
  ]);

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    }
  }, [
    releveData.vBags_Plastique,
    releveData.vBags_Caoutchouc,
    releveData.vBags_Bois,
    releveData.vBags_Textile,
    releveData.vBags_Carton,
    releveData.vBags_Metal,
    releveData.vBags_Verre,
    releveData.vBags_Unknown,
  ]);

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    } else {
      const calcVolTot = computeTotalVolume();
      if (currentUnit == UNITS.percentage && calcVolTot > 0) {
        Object.keys(listeMateriaux).map((key, index) =>
          updateFraction(key, calcVolTot, index)
        );
      }
    }
  }, [currentUnit]);

  function checkPercentagesConsistency(isConversion) {
    if (currentUnit == UNITS.percentage) {
      const delta = 0.1; // pour gérer les arrondis

      const sum_percentages = Object.keys(listeMateriaux).reduce((acc, mat) => {
        const materialName = "fracBags_" + mat;
        return acc + (releveData[materialName] ? releveData[materialName] : 0);
      }, 0);

      if (releveData.vBags_TOTAL != null && releveData.vBags_TOTAL != 0) {
        if (sum_percentages + delta < 100 || sum_percentages - delta > 100) {
          if (isConversion) {
            throw "Pour passer d'une saisie en % à une saisie en L, la somme des pourcentages des matériaux doit être égale à 100%. Ajustez les pourcentages ou supprimez le volume total pour revenir à la saisie en L.";
          } else {
            if (sum_percentages != 0) {
              throw "La somme des pourcentages des matériaux doit être égale à 100%. Ajustez les pourcentages ou supprimez les pour continuer.";
            }
          }
        }
      } else {
        if (sum_percentages != 0) {
          throw "Vous n'avez pas indiqué le volume total.";
        }
      }
    }
  }

  function checkVolTot() {
    if (releveData.vBags_TOTAL <= 0 || isNaN(releveData.vBags_TOTAL)) {
      throw "Merci d'indiquer un volume.";
    }
  }

  return (
    <Container>
      <View style={s.page_title}>
        <Text style={s.titre_h1}>Volumes des sacs par matériau</Text>
      </View>

      <ScrollView>
        <View style={s.txt_normal}>
          <Text>
            Estimez ici les volumes de chaque matériau ramassé. Pour vous aider,
            utilisez des sacs de taille standard. Si vous ne pouvez pas estimer
            ces volumes en Litres, vous avez la possibilité de rentrer un volume
            total et des pourcentages pour chaque matériau.
          </Text>
        </View>

        <Text style={[s.titre_h2, s.label_h2_form]}>
          Volume total des sacs en litres :
        </Text>

        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View style={{ flex: 1 }}>
            <View
              style={[s.container_input_unit, { justifyContent: "center" }]}
            >
              <TextInput
                style={
                  currentUnit != UNITS.percentage
                    ? [
                        s.titre_h2,
                        s.label_h2_input,
                        {
                          width: 130,
                          color: s.mer_terre_blue_color,
                          backgroundColor: "transparent",
                        },
                      ]
                    : [s.titre_h2, s.label_h2_input, { width: 130 }]
                }
                onChangeText={(e) => {
                  saveReleveData(e, "vBags_TOTAL");
                }}
                placeholder="--"
                keyboardType="numeric"
                maxLength={8}
                editable={currentUnit != UNITS.percentage ? false : true}
                value={
                  releveData.vBags_TOTAL
                    ? (
                        Math.round(releveData.vBags_TOTAL * 100) / 100
                      ).toString()
                    : null
                }
              />
              <Text style={[s.titre_h2, s.input_unit]}>L</Text>
            </View>
          </View>

          <View style={{ flex: 1 }}>
            <View
              style={[
                s.txt_normal,
                { flexDirection: "column", alignItems: "center" },
              ]}
            >
              <Switch
                //trackColor={{false: '#767577', true: '#81b0ff'}}
                //thumbColor={isPercent ? '#f5dd4b' : '#f4f3f4'}
                //ios_backgroundColor="#3e3e3e"
                onValueChange={() => {
                  try {
                    checkPercentagesConsistency(true);
                    toggleSwitch();
                  } catch (e) {
                    Alert.alert("Attention !", e);
                  }
                }}
                value={currentUnit}
              />
              {currentUnit == UNITS.percentage ? (
                <>
                  <Text>Saisie en pourcentage</Text>
                </>
              ) : (
                <>
                  <Text>Saisie en litres</Text>
                </>
              )}
            </View>
          </View>
        </View>

        <Text style={[s.titre_h2, s.label_h2_form, { marginTop: 15 }]}>
          Répartition du volume par matériau :
        </Text>

        <View style={{ marginLeft: 10 }}>
          {Object.keys(listeMateriaux).map((key, index) =>
            inputMeasure(key, currentUnit, "Bags_", index)
          )}
        </View>
      </ScrollView>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 10,
        }}
      >
        <ButtonNav
          onPress={goToCountWastes}
          textButton={"Retour"}
          lengthButton={120}
        ></ButtonNav>
        <ButtonNav
          onPress={() => {
            try {
              checkVolTot();
              try {
                checkPercentagesConsistency(false);
                goToWeightBags();
              } catch (e) {
                Alert.alert("Attention !", e);
              }
            } catch (e) {
              Alert.alert("Champ obligatoire", e);
            }
          }}
          textButton={"Continuer"}
          lengthButton={120}
        ></ButtonNav>
      </View>
    </Container>
  );
}
