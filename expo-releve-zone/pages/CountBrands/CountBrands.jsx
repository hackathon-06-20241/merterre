import { s, modalStyles } from "../../styles/styles";
import { BRANDS } from "../../Data/References/Brands";
import { Container } from "../../components/Container/Container";
import {
  Text,
  View,
  ScrollView,
  Switch,
  TextInput,
  Alert,
  Button,
  Modal,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useState, useEffect, useContext } from "react";
import { BrandCounter } from "../../components/Counter/BrandCounter";
import { ButtonAdd } from "../../components/Button/ButtonAdd";
import { ButtonNav } from "../../components/Button/ButtonNav";
import { Picker } from "@react-native-picker/picker";
import "react-native-get-random-values";

import {
  NAV_MAIN_WASTES,
  NAV_STOCK_EVENT,
  normalizeString,
} from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";
import { SafeAreaView } from "react-native-safe-area-context";

export default function CountBrands() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const [isAddDialogVisible, setIsAddDialogVisible] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [brandList, setBrandList] = useState(null);

  const nav = useNavigation();

  function onCloseModal() {
    //console.log("onCloseModalStructure");
    setIsAddDialogVisible(false);
  }

  function manageBrandField(nomRecherche) {
    const nomRechercheNormalized = normalizeString(nomRecherche);
    setBrandList(
      BRANDS.filter((item) => {
        const itemNormalized = normalizeString(item);
        return (
          itemNormalized.startsWith(nomRechercheNormalized) ||
          itemNormalized.includes(nomRechercheNormalized)
        );
      })
    );
  }

  function goToVolumeMainWastes() {
    nav.navigate(NAV_MAIN_WASTES);
  }

  function goToStockEvent() {
    nav.navigate(NAV_STOCK_EVENT);
  }

  function renderBrandCountList() {
    const brandList = releveData.dataDechetsMarques;
    if (Object.keys(brandList).length > 0) {
      return brandList.map((brand, index) => (
        <View key={index} style={{ marginBottom: 15 }}>
          <BrandCounter brand={brand} deleteFunction={deleteBrand} />
        </View>
      ));
    }
  }

  function showAddDialog() {
    setIsAddDialogVisible(true);
    setBrandList(BRANDS);
  }

  function addBrand(item) {
    try {
      // Check if brand already there or not
      const alreadyExists = releveData.dataDechetsMarques.some(
        (brandAlreadySelected) => brandAlreadySelected.name === item
      );
      if (alreadyExists) return releveData;

      // Otherwise
      const newBrand = {
        name: item,
        number: 0,
      };

      setReleveData((releveData) => {
        return {
          ...releveData,
          dataDechetsMarques: [...releveData.dataDechetsMarques, newBrand].sort(
            (a, b) => {
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            }
          ),
        };
      });
    } finally {
      setIsAddDialogVisible(false);
    }
  }

  function deleteBrand(brandToDelete) {
    Alert.alert("Supression", "Supprimer cette marque?", [
      {
        text: "Supprimer",
        style: "destructive",
        onPress: () => {
          setReleveData((releveData) => {
            return {
              ...releveData,
              dataDechetsMarques: [
                ...releveData.dataDechetsMarques.filter(
                  (brand) => brand.name != brandToDelete.name
                ),
              ],
            };
          });
        },
      },
      {
        text: "Annuler",
        style: "cancel",
      },
    ]);
  }

  function checkCounts() {
    if (releveData.dataDechetsMarques.length > 0) {
      if (
        releveData.exhaustiviteMarques == null ||
        releveData.exhaustiviteMarques == 0
      ) {
        throw "Veuillez indiquer si vous avez compté toutes les marques ramassées.";
      }
    }
  }

  useEffect(() => {
    if (releveData.dataDechetsMarques.length == 0) {
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["exhaustiviteMarques"]: 0,
        };
      });
    }
  }, [releveData.dataDechetsMarques]);

  return (
    <>
      <Container>
        <View style={s.page_title}>
          <Text style={s.titre_h1}>Comptage des marques</Text>
        </View>

        <ScrollView>
          <View style={s.txt_normal}>
            <Text>
              A cette étape, vous devez indiquer le nombre d'objets retrouvés
              par marques. Si vous ne souhaitez pas saisir ces données vous
              pouvez valider directement. Appuyez sur le bouton d'ajout pour
              saisir une nouvelle marque, appuyez sur la marque si vous
              souhaitez la supprimer.
            </Text>
            <ButtonAdd
              onPress={showAddDialog}
              textButton={"+ Ajouter une marque"}
              lengthButton={220}
            />
          </View>

          {renderBrandCountList()}
        </ScrollView>

        {releveData.dataDechetsMarques.length > 0 && (
          <View style={[{ marginBottom: 10 }]}>
            <Text style={[s.titre_h2, s.label_h2_form]}>
              Avez vous compté toutes les marques ramassées? (*)
            </Text>

            <View style={s.picker_style}>
              <Picker
                style={s.picker_style}
                selectedValue={releveData.exhaustiviteMarques}
                onValueChange={(e) => {
                  saveReleveData(e, "exhaustiviteMarques");
                }}
              >
                <Picker.Item
                  label="Selectionnez une réponse"
                  value={null}
                  color="#aaa"
                />
                <Picker.Item
                  label="OUI, toutes les marques ramassées ont été comptés (boissons, emballages, objets divers)"
                  value="1"
                  color="#000"
                ></Picker.Item>
                <Picker.Item
                  label="NON, mais toutes les marques de bouteilles et canettes ont été comptées"
                  value="2"
                  color="#000"
                />
                <Picker.Item
                  label="NON, seulement quelques marques"
                  value="3"
                  color="#000"
                />
              </Picker>
            </View>
          </View>
        )}

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-evenly",
            marginVertical: 10,
          }}
        >
          <ButtonNav
            onPress={goToVolumeMainWastes}
            textButton={"Retour"}
            lengthButton={120}
          ></ButtonNav>
          <ButtonNav
            onPress={() => {
              try {
                checkCounts();
                goToStockEvent();
              } catch (e) {
                Alert.alert("Champ obligatoire", e);
              }
            }}
            textButton={"Continuer"}
            lengthButton={120}
          ></ButtonNav>
        </View>
      </Container>

      <Modal
        animationType="fade"
        transparent={true}
        visible={isAddDialogVisible}
        onRequestClose={onCloseModal}
      >
        <SafeAreaView style={modalStyles.centeredView}>
          <View style={modalStyles.modalView}>
            <TextInput
              style={[s.titre_h2, s.label_h2_input]}
              placeholder="Recherchez une marque "
              onChangeText={manageBrandField}
            />
            <FlatList
              data={brandList}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={modalStyles.option}
                  onPress={() => {
                    addBrand(item);
                  }}
                >
                  <Text style={[s.titre_h2, modalStyles.textStyle]}>
                    {item}
                  </Text>
                </TouchableOpacity>
              )}
              //keyExtractor={(item) => item}
            />
            <TouchableOpacity style={modalStyles.closeButton}>
              <ButtonNav
                textButton={"Fermer"}
                lengthButton={110}
                onPress={onCloseModal}
              ></ButtonNav>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </Modal>
    </>
  );
}
