import {
  ScrollView,
  Text,
  TextInput,
  View,
  Alert,
  Modal,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
} from "react-native";
import { format } from "date-fns";
import { Checkbox } from "expo-checkbox";
import { Container } from "../../components/Container/Container";
import { s, modalStyles } from "../../styles/styles";
import { useContext, useEffect, useState } from "react";
import { Picker } from "@react-native-picker/picker";
import { useNavigation } from "@react-navigation/native";
import { ButtonNav } from "../../components/Button/ButtonNav";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { ButtonAdd } from "../../components/Button/ButtonAdd";
import {
  TYPE_DECHET,
  TYPE_LIEU,
  TYPE_MILIEU,
  TAB_LIEUX_MILIEUX,
} from "../../Data/References/types-lieux";
import { TAB_PROJETS_ENVERGURE } from "../../Data/References/projets-envergures";
import {
  NAV_HOME,
  NAV_PROTOCOL_CHOICE,
  checkIsFieldEmpty,
  removeAccents,
} from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";
import { STRUCTURES } from "../../Data/References/Structures";
import { SafeAreaView } from "react-native-safe-area-context";

import trashIcon from "../../assets/icone_poubelle.png";

export default function GeneralInfo() {
  const nav = useNavigation();

  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const mois = [
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre",
  ];

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleDateConfirm = (date) => {
    try {
      checkDate(date);
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["evtDate"]: toFrenchDate(date),
        };
      });
    } catch (e) {
      Alert.alert("Attention !", e);
    }
    hideDatePicker();
  };

  function updateLinear() {
    const get_linear = TAB_LIEUX_MILIEUX.filter(
      (group) =>
        group.id_milieu == releveData.typeMilieu &&
        group.id_lieu == releveData.typeLieu
    )[0]
      ? TAB_LIEUX_MILIEUX.filter(
          (group) =>
            group.id_milieu == releveData.typeMilieu &&
            group.id_lieu == releveData.typeLieu
        )[0].linear_data
      : null;

    if (get_linear == false) {
      setReleveData((releveData) => {
        return {
          ...releveData,
          ["linearCoords"]: [],
        };
      });
    }
  }

  function setCheckBox(tickField, fieldName, message) {
    return (
      <View>
        <TouchableOpacity
          style={s.checkbox_container}
          onPress={() => {
            saveReleveData(!tickField, fieldName);
          }}
        >
          <Checkbox
            color={s.mer_terre_blue_color}
            style={s.checkbox_style}
            value={tickField}
          />
          <Text
            style={[
              s.titre_h2,
              s.label_check_box,
              { flex: 1, flexWrap: "wrap" },
            ]}
          >
            {message}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  useEffect(() => {
    if (releveData.linearCoords?.length > 0) updateLinear();
  }, [releveData.typeLieu]);

  function toFrenchDate(dateObj) {
    return format(dateObj, "dd/MM/yyyy");
  }

  function retourHome() {
    nav.navigate(NAV_HOME);
  }

  function goToProtocolChoice() {
    nav.navigate(NAV_PROTOCOL_CHOICE);
  }

  function checkFields() {
    if (
      checkIsFieldEmpty(releveData.structure) ||
      checkIsFieldEmpty(releveData.evtName) ||
      checkIsFieldEmpty(releveData.evtDate) ||
      checkIsFieldEmpty(releveData.typeMilieu) ||
      checkIsFieldEmpty(releveData.typeLieu) ||
      checkIsFieldEmpty(releveData.typeDechet) ||
      checkIsFieldEmpty(releveData.nbParticipants)
    ) {
      throw "Veuillez compléter les champs marqués d'une astérisque pour continuer.";
    }
  }

  function checkDate(date) {
    const today = new Date();
    if (date > today) {
      throw "Veuillez ne pas indiquer une date postérieure à la date du jour .";
    }
  }

  useEffect(() => {
    setReleveData((releveData) => {
      return {
        ...releveData,
        ["typeLieu"]: null,
      };
    });
  }, [releveData.typeMilieu]);

  useEffect(() => {
    setReleveData((releveData) => {
      return {
        ...releveData,
        ["typeDechet"]: null,
      };
    });
  }, [releveData.typeMilieu, releveData.typeLieu]);

  useEffect(() => {
    saveReleveData(true, "releveInProgress");
  }, []);

  const [modalVisible, setModalVisible] = useState(false);
  const [filteredStructure, setFilteredStructure] = useState(null);
  const [firstModal, setFirstModal] = useState(true);
  const [structureSelected, setStructureSelected] = useState(null);

  function onCloseModalStructure() {
    //console.log("onCloseModalStructure");
    setModalVisible(false);
  }

  function onSelectItemStructure(item) {
    //console.log("onSelectItemStructure :" + item.nom + ":" + item.id);
    setStructureSelected(item);
  }

  function manageStructureField(nomRecherche) {
    //console.log("nomRecherche:" + nomRecherche);
    if (nomRecherche === "*") {
      setFilteredStructure(
        STRUCTURES.sort((a, b) => a.nom.localeCompare(b.nom))
      );
    } else if (nomRecherche?.length >= 1) {
      const filteredList = STRUCTURES.filter((item) =>
        removeAccents(item.nom?.toLowerCase().trim()).startsWith(
          removeAccents(nomRecherche)
        )
      );

      //console.log("Taille liste:" + filteredList.length);

      if (filteredList.length <= 15 || nomRecherche?.length >= 3) {
        setFilteredStructure(
          filteredList.sort((a, b) => a.nom.localeCompare(b.nom))
        );
      }
    }
  }

  useEffect(() => {
    if (!firstModal) {
      if (filteredStructure.length > 0) {
        setModalVisible(true);
      }
    } else {
      setFirstModal(false);
    }
  }, [filteredStructure]);

  useEffect(() => {
    if (structureSelected) {
      saveReleveData(structureSelected, "structure");
      setModalVisible(false);
    }
  }, [structureSelected]);

  useEffect(() => {
    saveReleveData(true, "releveInProgress");
  }, []);

  return (
    <Container>
      <View style={s.page_title}>
        <Text style={s.titre_h1}>Informations générales</Text>
      </View>
      <ScrollView style={{ display: "flex" }}>
        {/* Nom de l evenement */}
        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Sélection de la structure (*) :
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <TextInput
              style={[s.titre_h2, s.label_h2_input, { flex: 1 }]}
              value={releveData.structure?.nom}
              onChangeText={manageStructureField}
              placeholder="Recherchez avec 3 lettres ou *"
              editable={!releveData?.structure}
            />
            {releveData.structure && (
              <TouchableOpacity
                style={{ flex: 0.2 }}
                onPress={() => saveReleveData(null, "structure")}
              >
                <Image
                  style={{ width: 30, height: 30, marginLeft: 10 }}
                  source={trashIcon}
                />
              </TouchableOpacity>
            )}
          </View>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={onCloseModalStructure}
          >
            <SafeAreaView style={modalStyles.centeredView}>
              <View style={modalStyles.modalView}>
                <FlatList
                  data={filteredStructure}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      style={modalStyles.option}
                      onPress={() => {
                        onSelectItemStructure(item);
                      }}
                    >
                      <Text style={[s.titre_h2, modalStyles.textStyle]}>
                        {item.nom}
                      </Text>
                    </TouchableOpacity>
                  )}
                  keyExtractor={(item) => item.id.toString()}
                />
                <TouchableOpacity style={modalStyles.closeButton}>
                  <ButtonNav
                    textButton={"Fermer"}
                    lengthButton={110}
                    onPress={onCloseModalStructure}
                  ></ButtonNav>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </Modal>
        </View>

        {/* Nom de l evenement */}
        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Nom de l'évènement (*) :
          </Text>
          <TextInput
            style={[s.titre_h2, s.label_h2_input]}
            value={releveData.evtName}
            onChangeText={(e) => {
              saveReleveData(e, "evtName");
            }}
            placeholder="Nom de l'évènement"
          />
        </View>

        {/* Date de début de l evenement */}
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Date de l'événement (*) :
          </Text>
          <ButtonAdd
            onPress={showDatePicker}
            textButton={releveData.evtDate ? releveData.evtDate : "Cliquez"}
            lengthButton={140}
            styleSup={{ position: "absolute", left: 215 }}
          ></ButtonAdd>
          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={handleDateConfirm}
            onCancel={hideDatePicker}
          />
        </View>

        {/* Détail Evt */}
        <View>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Caractéristiques de l'événement (options à cocher) :
          </Text>
          {setCheckBox(
            releveData.adopt1spot,
            "adopt1spot",
            "Cet événement s'inscrit dans une initiative Adopt'1 Spot"
          )}
          {/* Envergure 
          TODO prévoir d exporter la liste en JSON */}
          <View>
            {setCheckBox(
              releveData.projetEnvergure,
              "projetEnvergure",
              "Cet événement fait partie d'un événement d'envergure"
            )}

            {releveData.projetEnvergure && (
              <View style={s.picker_style}>
                <Picker
                  selectedValue={releveData.projetEnvergure}
                  onValueChange={(e) => {
                    saveReleveData(e, "projetEnvergure");
                  }}
                >
                  <Picker.Item
                    label="Selectionnez un événement d'envergure"
                    value={null}
                    color="#aaa"
                  />
                  {TAB_PROJETS_ENVERGURE.map((elem) => {
                    return (
                      <Picker.Item
                        label={elem.nom_ee}
                        value={elem.id}
                        key={elem.id}
                        color="#000"
                      />
                    );
                  })}
                </Picker>
              </View>
            )}
          </View>

          {/* Type de milieu */}
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Sélectionnez un type de milieu (*) :
          </Text>
          <View style={s.picker_style}>
            <Picker
              selectedValue={releveData.typeMilieu}
              onValueChange={(e) => {
                saveReleveData(e, "typeMilieu");
              }}
            >
              <Picker.Item
                label="Selectionner une option"
                value={null}
                color="#aaa"
              />
              {Object.keys(TYPE_MILIEU).map((key) => {
                return (
                  <Picker.Item
                    label={TYPE_MILIEU[key]}
                    value={key}
                    key={key}
                    color="#000"
                  />
                );
              })}
            </Picker>
          </View>

          {/* Type de lieu*/}
          {releveData.typeMilieu != null && (
            <View>
              <Text style={[s.titre_h2, s.label_h2_form]}>
                Sélectionnez un type de lieu (*) :
              </Text>
              <View style={s.picker_style}>
                <Picker
                  selectedValue={releveData.typeLieu}
                  onValueChange={(e) => {
                    saveReleveData(e, "typeLieu");
                  }}
                >
                  <Picker.Item
                    label="Selectionner une option"
                    value={null}
                    color="#aaa"
                  />
                  {TAB_LIEUX_MILIEUX.filter(
                    (group) => group.id_milieu == releveData.typeMilieu
                  ).map((elem) => {
                    return (
                      <Picker.Item
                        label={TYPE_LIEU[elem.id_lieu]}
                        value={elem.id_lieu}
                        key={elem.id_lieu}
                        color="#000"
                      />
                    );
                  })}
                </Picker>
              </View>
            </View>
          )}

          {/* Type de déchet */}
          {releveData.typeMilieu != null && releveData.typeLieu != null && (
            <View>
              <Text style={[s.titre_h2, s.label_h2_form]}>
                Sélectionnez un type de déchet (*) :
              </Text>
              <View style={s.picker_style}>
                <Picker
                  selectedValue={releveData.typeDechet}
                  onValueChange={(e) => {
                    saveReleveData(e, "typeDechet");
                  }}
                >
                  <Picker.Item
                    label="Selectionner une option"
                    value={null}
                    color="#aaa"
                  />

                  {TAB_LIEUX_MILIEUX.filter(
                    (group) =>
                      group.id_milieu == releveData.typeMilieu &&
                      group.id_lieu == releveData.typeLieu
                  )[0]
                    ? TAB_LIEUX_MILIEUX.filter(
                        (group) =>
                          group.id_milieu == releveData.typeMilieu &&
                          group.id_lieu == releveData.typeLieu
                      )[0].id_dechet.map((elem) => {
                        return (
                          <Picker.Item
                            label={TYPE_DECHET[elem]}
                            value={elem}
                            key={elem}
                            color="#000"
                          />
                        );
                      })
                    : null}
                </Picker>
              </View>
            </View>
          )}

          <View>
            <Text style={[s.titre_h2, s.label_h2_form]}>
              Nombre de participants (*) :
            </Text>
            <TextInput
              style={[s.titre_h2, s.label_h2_input]}
              value={releveData.nbParticipants}
              onChangeText={(e) => {
                saveReleveData(
                  e ? Math.round(e).toString() : null,
                  "nbParticipants"
                );
              }}
              placeholder="Nombre de participants"
              keyboardType="decimal-pad"
            />
          </View>

          <View>
            <Text style={[s.titre_h2, s.label_h2_form]}>
              Commentaire sur l'opération :
            </Text>
            <TextInput
              style={[s.titre_h2, s.label_h2_input]}
              value={releveData.commentaire}
              onChangeText={(e) => {
                saveReleveData(e, "commentaire");
              }}
              placeholder="Exemples : autres structures participantes, partenaires, nombre de participants par structures..."
              multiline={true}
              numberOfLines={3}
            />
          </View>
        </View>
      </ScrollView>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 10,
        }}
      >
        <ButtonNav
          onPress={retourHome}
          textButton={"Retour"}
          lengthButton={120}
        ></ButtonNav>
        <ButtonNav
          onPress={() => {
            try {
              checkFields();
              goToProtocolChoice();
            } catch (e) {
              Alert.alert("Champs obligatoires", e);
            }
          }}
          textButton={"Continuer"}
          lengthButton={120}
        ></ButtonNav>
      </View>
    </Container>
  );
}

// --- Champ supprimé
/*<View>
<Text style={[s.titre_h2, s.label_h2_form]}>
  Nombre de participants externes aux structures :
</Text>
<TextInput
  style={[s.titre_h2, s.label_h2_input]}
  value={releveData.nbPartExt}
  onChangeText={(e) => {
    saveReleveData(e, "nbPartExt");
  }}
  placeholder="Nombre de participants externes"
  keyboardType="decimal-pad"
/>
</View>*/
