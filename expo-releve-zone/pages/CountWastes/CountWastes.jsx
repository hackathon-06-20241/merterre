import { s, modalStyles } from "../../styles/styles";
import { Container } from "../../components/Container/Container";
import {
  Text,
  View,
  ScrollView,
  Alert,
  FlatList,
  Modal,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { WasteCounter } from "../../components/Counter/WasteCounter";
import { OtherWastes } from "../../components/Counter/OtherWastes";
import { useState, useEffect, useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import { Picker } from "@react-native-picker/picker";
import { ButtonNav } from "../../components/Button/ButtonNav";
import { ButtonAdd } from "../../components/Button/ButtonAdd";
import { SafeAreaView } from "react-native-safe-area-context";

import { WASTES_COUNTING_BY_PROTOCOL } from "../../Data/References/wastes-protocols";
import {
  TYPE_ITEMS,
  PLASTIC_ITEMS,
  WASTES_COUNTING_COMPLEX,
} from "../../Data/References/wastes-complex";
import { DECHETS_AUTRES } from "../../Data/References/dechets_autres";

import {
  NAV_PROTOCOL_CHOICE,
  NAV_VOLUME_BAGS,
  normalizeString,
} from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";
let isFirstRender = true;

export default function CountWastes() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const [wasteList, setWasteList] = useState(WASTES_COUNTING_BY_PROTOCOL);
  const [allWastes, setAllWastes] = useState(WASTES_COUNTING_COMPLEX);
  const [otherWastesList, setOtherWastes] = useState(DECHETS_AUTRES);
  const [wasteType, setWasteType] = useState();
  const [isAddDialogVisible, setIsAddDialogVisible] = useState(false);

  const typeCounts = {
    OBLIGATORY: "obligatoires",
    RECOMMENDED: "recommandés",
    OPTIONAL: "optionnels",
  };
  const nav = useNavigation();

  function goToProtocolChoice() {
    nav.navigate(NAV_PROTOCOL_CHOICE);
  }

  function goToVolumeBags() {
    nav.navigate(NAV_VOLUME_BAGS);
  }

  function getFilteredWasteList({ pLevel, usage }) {
    switch (pLevel) {
      case 1:
        return wasteList.filter((waste) => waste.label_p1 == usage);
      case 2:
        return wasteList.filter((waste) => waste.label_p2 == usage);
      case 3:
        return wasteList.filter((waste) => waste.label_p3 == usage);
      default: {
        if (!isFirstRender) {
          Alert.alert("Protocole inconnu");
        }
      }
    }
  }

  function displayCounter(index, waste) {
    return (
      <View key={index} style={{ marginBottom: 15 }}>
        <WasteCounter waste={waste} />
      </View>
    );
  }

  function displayCounterOtherWastes(index, waste) {
    return (
      <View key={index} style={{ marginBottom: 15 }}>
        <OtherWastes waste={waste} deleteFunction={deleteWaste} />
      </View>
    );
  }

  function displayCounter_P4(index, waste) {
    return (
      <View key={index} style={{ marginBottom: 15 }}>
        <WasteCounter waste={waste} />
      </View>
    );
  }

  function displayCountersByWasteType() {
    return (
      <View>
        <View style={{ flexDirection: "column", justifyContent: "center" }}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 10,
              marginHorizontal: 20,
            }}
          >
            <Text
              style={{
                width: 100,
                color: s.mer_terre_blue_color,
                fontSize: 18,
                fontWeight: "bold",
              }}
            >
              Catégorie :{" "}
            </Text>
            <View style={[s.picker_style, { flex: 1 }]}>
              <Picker selectedValue={wasteType} onValueChange={setWasteType}>
                <Picker.Item
                  label="Selectionner une catégorie de déchet"
                  value={null}
                  color="#aaa"
                />
                {Object.keys(TYPE_ITEMS).map((key) => {
                  return (
                    <Picker.Item
                      label={TYPE_ITEMS[key]}
                      value={key}
                      key={key}
                      color="#000"
                    />
                  );
                })}
              </Picker>
            </View>
          </View>

          <View style={{ marginTop: 10 }}>
            {wasteType != "PLASTIQUE" ? (
              allWastes
                .filter((group) => group.type == wasteType)
                .map((waste, index) => displayCounter_P4(index, waste))
            ) : (
              <View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginHorizontal: 20,
                  }}
                >
                  <Text
                    style={{
                      width: 100,
                      color: s.mer_terre_blue_color,
                      fontSize: 18,
                      fontWeight: "bold",
                    }}
                  >
                    Sous-type :{" "}
                  </Text>
                  <View style={[s.picker_style, { flex: 1 }]}>
                    <Picker
                      selectedValue={releveData.plasticType}
                      onValueChange={(e) => {
                        saveReleveData(e, "plasticType");
                      }}
                    >
                      <Picker.Item
                        label="Selectionner un type de plastique"
                        value={null}
                        color="#aaa"
                      />
                      {Object.keys(PLASTIC_ITEMS).map((key) => {
                        return (
                          <Picker.Item
                            label={PLASTIC_ITEMS[key]}
                            value={key}
                            key={key}
                            color="#000"
                          />
                        ); //if you have a bunch of keys value pair
                      })}
                    </Picker>
                  </View>
                </View>

                <View style={{ marginTop: 20 }}>
                  {allWastes
                    .filter((group) => group.type == wasteType)
                    .filter((group) => group.subtype == releveData.plasticType)
                    .map((waste, index) => displayCounter_P4(index, waste))}
                </View>
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }

  function displayCountersByUsage(index, pLevel, usage) {
    const filterList = getFilteredWasteList({ pLevel, usage });
    if (filterList.length > 0) {
      return (
        <View key={index}>
          <Text style={[s.titre_h2, s.label_h2_form]}>
            Comptages {typeCounts[usage]}
          </Text>
          {filterList.map((waste, index) => displayCounter(index, waste))}
        </View>
      );
    }
  }

  function renderWasteCountList(pickupLevel) {
    if (!isNaN(pickupLevel) && !isNaN(parseInt(pickupLevel))) {
      const pLevel = parseInt(pickupLevel);
      if (pLevel <= 3) {
        // Protocoles 1, 2 et 3
        return Object.keys(typeCounts).map((usage, index) =>
          displayCountersByUsage(index, pLevel, usage)
        );
      } else {
        // Protocole 4
        return displayCountersByWasteType();
      }
    }
  }

  function showAddDialog() {
    setIsAddDialogVisible(true);
    setOtherWastes(DECHETS_AUTRES.map((waste) => waste.name));
  }

  function onCloseModal() {
    //console.log("onCloseModalStructure");
    setIsAddDialogVisible(false);
  }

  function addWaste(item) {
    try {
      // Check if waste already there or not
      const alreadyExists = releveData.dataDechetsAutres.some(
        (brandAlreadySelected) => brandAlreadySelected.name === item
      );
      if (alreadyExists) return releveData;

      // Otherwise
      const newWaste = {
        dcsmm: DECHETS_AUTRES.filter((waste) => waste.name == item)[0].dcsmm,
        number: 0,
      };

      setReleveData((releveData) => {
        return {
          ...releveData,
          dataDechetsAutres: [...releveData.dataDechetsAutres, newWaste].sort(
            (a, b) => {
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            }
          ),
        };
      });
    } finally {
      setIsAddDialogVisible(false);
    }
  }

  function deleteBrand(brandToDelete) {
    Alert.alert("Supression", "Supprimer cette marque?", [
      {
        text: "Supprimer",
        style: "destructive",
        onPress: () => {
          setReleveData((releveData) => {
            return {
              ...releveData,
              dataDechetsAutres: [
                ...releveData.dataDechetsAutres.filter(
                  (brand) => brand.name != brandToDelete.name
                ),
              ],
            };
          });
        },
      },
      {
        text: "Annuler",
        style: "cancel",
      },
    ]);
  }

  function deleteWaste(wasteToDelete) {
    Alert.alert("Supression", "Supprimer ce déchet?", [
      {
        text: "Supprimer",
        style: "destructive",
        onPress: () => {
          setReleveData((releveData) => {
            return {
              ...releveData,
              dataDechetsAutres: [
                ...releveData.dataDechetsAutres.filter(
                  (waste) => waste.dcsmm != wasteToDelete.dcsmm
                ),
              ],
            };
          });
        },
      },
      {
        text: "Annuler",
        style: "cancel",
      },
    ]);
  }

  function countOtherWastes(pickupLevel) {
    if (!isNaN(pickupLevel) && !isNaN(parseInt(pickupLevel))) {
      const pLevel = parseInt(pickupLevel);
      const otherWastes = releveData.dataDechetsAutres;
      if (pLevel <= 3) {
        return (
          <View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "space-evenly",
                alignItems: "center",
              }}
            >
              <Text style={[s.titre_h2, s.label_h2_form]}>Autres déchets</Text>
              <ButtonAdd
                onPress={showAddDialog}
                textButton={"+ Ajouter un déchet"}
                lengthButton={200}
              />
            </View>
            {Object.keys(otherWastes).length > 0 &&
              otherWastes.map((waste, index) =>
                displayCounterOtherWastes(index, waste)
              )}
          </View>
        );
      }
    }
  }

  function manageWasteField(nomRecherche) {
    const nomRechercheNormalized = normalizeString(nomRecherche);
    setOtherWastes(
      DECHETS_AUTRES.map((waste) => waste.name).filter((item) => {
        const itemNormalized = normalizeString(item);
        return (
          itemNormalized.startsWith(nomRechercheNormalized) ||
          itemNormalized.includes(nomRechercheNormalized)
        );
      })
    );
  }

  return (
    <>
      <Container>
        <View style={[s.page_title]}>
          <Text style={s.titre_h1}>Comptage des déchets</Text>
        </View>

        <ScrollView>
          {renderWasteCountList(releveData.pickupLevel)}
          {countOtherWastes(releveData.pickupLevel)}
        </ScrollView>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-evenly",
            marginVertical: 10,
          }}
        >
          <ButtonNav
            onPress={goToProtocolChoice}
            textButton={"Retour"}
            lengthButton={120}
          ></ButtonNav>
          <ButtonNav
            onPress={goToVolumeBags}
            textButton={"Continuer"}
            lengthButton={120}
          ></ButtonNav>
        </View>
      </Container>

      <Modal
        animationType="fade"
        transparent={true}
        visible={isAddDialogVisible}
        onRequestClose={onCloseModal}
      >
        <SafeAreaView style={modalStyles.centeredView}>
          <View style={modalStyles.modalView}>
            <TextInput
              style={[s.titre_h2, s.label_h2_input]}
              placeholder="Recherchez un déchet "
              onChangeText={manageWasteField}
            />
            <FlatList
              data={otherWastesList}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={modalStyles.option}
                  onPress={() => {
                    addWaste(item);
                  }}
                >
                  <Text style={[s.titre_h2, modalStyles.textStyle]}>
                    {item}
                  </Text>
                </TouchableOpacity>
              )}
              //keyExtractor={(item) => item}
            />
            <TouchableOpacity style={modalStyles.closeButton}>
              <ButtonNav
                textButton={"Fermer"}
                lengthButton={110}
                onPress={onCloseModal}
              ></ButtonNav>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </Modal>
    </>
  );
}
