import { s } from "../../styles/styles";
import { Container } from "../../components/Container/Container";
import {
  Text,
  View,
  ScrollView,
  TextInput,
  Alert,
  TouchableOpacity,
} from "react-native";
import { useState, useEffect, useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import { Checkbox } from "expo-checkbox";
import { InputNumber } from "../../components/InputNumber/InputNumber";
import { InputBagVolume } from "../../components/InputNumber/InputBagVolume";
import { ButtonNav } from "../../components/Button/ButtonNav";

import { NAV_VOLUME_BAGS, NAV_MAIN_WASTES } from "../../Data/constants";
import ReleveDataContext from "../../Contexts/ReleveDataContext";

import { listeMateriaux } from "../../Data/constants";

let isFirstRender = true;

export default function WeightBags() {
  const { releveData, setReleveData, saveReleveData } =
    useContext(ReleveDataContext);

  const nav = useNavigation();

  function goToVolumeBags() {
    nav.navigate(NAV_VOLUME_BAGS);
  }

  function goToVolumeMainWastes() {
    nav.navigate(NAV_MAIN_WASTES);
  }

  function computeTotalWeight() {
    return (calcTot =
      Math.round(
        Object.keys(listeMateriaux).reduce((acc, mat) => {
          const materialName = "wBags_" + mat;
          return (
            acc + (releveData[materialName] ? releveData[materialName] : 0)
          );
        }, 0) * 100
      ) / 100);
  }

  function resetWeight(mat, index) {
    const fieldname = "wBags_" + mat;
    setReleveData((releveData) => {
      return {
        ...releveData,
        [fieldname]: null,
      };
    });
  }

  function switchToDetailed() {
    if (!isFirstRender && releveData.detailedWeight) {
      if (releveData.wBags_TOTAL > 0) {
        Alert.alert("Attention", "La saisie des poids détaillés sera perdue.", [
          {
            text: "Annuler",
            style: "cancel",
          },
          {
            text: "Continuer",
            style: "destructive",
            onPress: () => {
              saveReleveData(false, "detailedWeight");
              [...Object.keys(listeMateriaux), "TOTAL"].map((key, index) =>
                resetWeight(key, index)
              );
            },
          },
        ]);
      } else {
        saveReleveData(false, "detailedWeight");
      }
    } else {
      saveReleveData(true, "detailedWeight");
    }
  }

  function inputMeasure(mat, unit, typeMeas, index) {
    const fieldName = "w" + typeMeas + mat;
    return (
      <InputBagVolume
        key={index}
        onChangeNumber={(e) => {
          saveReleveData(e, fieldName);
        }}
        number={releveData[fieldName]}
        material={mat}
        inputTitle={listeMateriaux[mat]}
        unit={unit}
        typeMeas={typeMeas}
      ></InputBagVolume>
    );
  }

  useEffect(() => {
    if (isFirstRender) {
      isFirstRender = false;
    } else {
      if (releveData.detailedWeight) {
        const calcTotWeight = computeTotalWeight();
      }
    }
  }, [
    releveData.detailedWeight,
    releveData.wBags_Plastique,
    releveData.wBags_Caoutchouc,
    releveData.wBags_Bois,
    releveData.wBags_Textile,
    releveData.wBags_Carton,
    releveData.wBags_Metal,
    releveData.wBags_Verre,
    releveData.wBags_Unknown,
  ]);

  function checkTotWeight() {
    if (releveData.wBags_TOTAL <= 0) {
      throw "Merci d'indiquer un poids.";
    }
  }

  function isNumeric(string) {
    return /^[+-]?\d+(\.\d+)?$/.test(string);
  }

  function setTotal() {
    return (
      <View
        style={[
          s.container_input_unit,
          { justifyContent: "left", marginLeft: 10 },
        ]}
      >
        <TextInput
          style={
            releveData.detailedWeight
              ? [
                  s.titre_h2,
                  s.label_h2_input,
                  {
                    width: 130,
                    color: s.mer_terre_blue_color,
                    backgroundColor: "transparent",
                  },
                ]
              : [s.titre_h2, s.label_h2_input, { width: 130 }]
          }
          inputmode="decimal"
          placeholder="--"
          keyboardType="numeric"
          maxLength={8}
          editable={releveData.detailedWeight ? false : true}
          onChangeText={(textValue) => {
            const strNum = textValue.replace(",", ".");
            const [whole, decimal] = strNum.split(".");
            const number_to_set =
              textValue == ""
                ? 0
                : decimal == ""
                ? parseFloat(whole)
                : parseFloat(strNum);

            if (isNumeric(strNum)) {
              saveReleveData(number_to_set, "wBags_TOTAL");
            } else {
              saveReleveData(strNum, "wBags_TOTAL");
            }
          }}
          value={
            releveData.wBags_TOTAL
              ? isNumeric(releveData.wBags_TOTAL)
                ? (Math.round(releveData.wBags_TOTAL * 100) / 100).toString()
                : releveData.wBags_TOTAL
              : null
          }
        />
        <Text style={[s.titre_h2, s.input_unit]}>kg</Text>
      </View>
    );
  }

  return (
    <Container>
      <View style={s.page_title}>
        <Text style={s.titre_h1}>Poids des sacs par matériau</Text>
      </View>

      <ScrollView>
        <View style={s.txt_normal}>
          <Text>
            Vous avez une balance ou un peson, vous pouvez donc indiquer le
            poids total des déchets ramassés ou saisir le détail par matériau.
          </Text>
        </View>

        <Text style={[s.titre_h2, s.label_h2_form]}>
          Poids total des sacs :
        </Text>

        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View style={{ flex: 1 }}>{setTotal()}</View>
        </View>

        <View>
          <TouchableOpacity
            style={s.checkbox_container}
            onPress={() => {
              switchToDetailed();
            }}
          >
            <Checkbox
              color={s.mer_terre_blue_color}
              style={s.checkbox_style}
              value={releveData.detailedWeight}
            />
            <Text
              style={[
                s.titre_h2,
                s.label_check_box,
                { flex: 1, flexWrap: "wrap" },
              ]}
            >
              Saisir la répartition du poids par matériau
            </Text>
          </TouchableOpacity>
        </View>

        {!!releveData.detailedWeight && (
          <View style={{ marginLeft: 10 }}>
            {Object.keys(listeMateriaux).map((key, index) =>
              inputMeasure(key, "kg", "Bags_", index)
            )}
          </View>
        )}
      </ScrollView>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 10,
        }}
      >
        <ButtonNav
          onPress={goToVolumeBags}
          textButton={"Retour"}
          lengthButton={120}
        ></ButtonNav>
        <ButtonNav
          onPress={() => {
            try {
              checkTotWeight();
              goToVolumeMainWastes();
            } catch (e) {
              Alert.alert("Champ obligatoire", e);
            }
          }}
          textButton={"Continuer"}
          lengthButton={120}
        ></ButtonNav>
      </View>
    </Container>
  );
}
