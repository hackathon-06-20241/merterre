// Indique les champs qui ne sont pas alimenté
const TODO = "<todo>";

/**
 * Contient la liste des informations du formulaire
 */
export let releveJson = {
  // Logic data
  releveInProgress: false,
  // Logic to store data
  pathFile: null,
  // General Info
  structure: null,
  evtName: null,
  evtDate: null,
  nbParticipants: null,
  nbPartExt: null,
  projetEnvergure: null,
  adopt1spot: false,
  typeMilieu: null,
  typeLieu: null,
  typeDechet: null,
  description: null,
  // CountWaste / Comptage des dechets
  pickupLevel: null,
  plasticType: null,
  dataDechetsIndicateurs: [],
  dataDechetsAutres: [],
  // VolumeBags
  vBags_TOTAL: null,
  vBags_Plastique: null,
  vBags_Caoutchouc: null,
  vBags_Bois: null,
  vBags_Textile: null,
  vBags_Carton: null,
  vBags_Metal: null,
  vBags_Verre: null,
  vBags_Unknown: null,
  fracBags_Plastique: null,
  fracBags_Caoutchouc: null,
  fracBags_Bois: null,
  fracBags_Textile: null,
  fracBags_Carton: null,
  fracBags_Metal: null,
  fracBags_Verre: null,
  fracBags_Unknown: null,
  // weightBags
  detailedWeight: null,
  wBags_TOTAL: null,
  wBags_Plastique: null,
  wBags_Caoutchouc: null,
  wBags_Bois: null,
  wBags_Textile: null,
  wBags_Carton: null,
  wBags_Metal: null,
  wBags_Verre: null,
  wBags_Unknown: null,
  // MainWastes
  detailedVolume: null,
  vMain_TOTAL: null,
  vMain_Plastique: null,
  vMain_Caoutchouc: null,
  vMain_Bois: null,
  vMain_Textile: null,
  vMain_Carton: null,
  vMain_Metal: null,
  vMain_Verre: null,
  vMain_Unknown: null,
  fracMain_Plastique: null,
  fracMain_Caoutchouc: null,
  fracMain_Bois: null,
  fracMain_Textile: null,
  fracMain_Carton: null,
  fracMain_Metal: null,
  fracMain_Verre: null,
  fracMain_Unknown: null,
  wMain_TOTAL: null,
  DV_comments: null,
  // CountBrands
  dataDechetsMarques: [],
  exhaustiviteMarques: null,
  // StockEvent
  zoneName: null,
  dureeH: null,
  surfaceTotale: null,
  linearDistance: null,
  commentaire: null,
  userLat: null,
  userLon: null,
  polygonCoords: [],
  linearCoords: [],
  centroid: [],
};

/**
 * Structure permettant
 */
export let releveJsonXLS = {
  STRUCTURE_NOM: "",
  STRUCTURE_ID: "",
  EV_NOM: "",
  EV_DESCR: "",
  EV_DATE_DEBUT: null,
  EV_DATE_FIN: null,
  EV_POINT_GPS_LAT: null,
  EV_POINT_GPS_LON: null,
  EV_NIVEAU: "",
  EV_ENVERGURE_ID: null,
  EV_INITIATIVES_IDS: [],
  EV_SPOT_NOM: "",
  EV_SPOT_ID: "",
  CONTEXTE_TYPE_MILIEU_UUID: "",
  CONTEXTE_TYPE_LIEU_UUID: "",
  CONTEXTE_TYPE_DECHET_UUID: "",
  CONTEXTE_NOM_ZONE: "",
  CONTEXTE_DUREE: null,
  CONTEXTE_PARTICIPANT_ORGA: null,
  CONTEXTE_PARTICIPANT_EXT: null,
  CONTEXTE_GEOMETRIE: null,
  DATA_GLOBAL_VOL_TOTAL: null,
  DATA_GLOBAL_VOL_PLASTIQUE: null,
  DATA_GLOBAL_VOL_CAOUTCHOUC: null,
  DATA_GLOBAL_VOL_BOIS: null,
  DATA_GLOBAL_VOL_TEXTILE: null,
  DATA_GLOBAL_VOL_PAPIER: null,
  DATA_GLOBAL_VOL_METAL: null,
  DATA_GLOBAL_VOL_VERRE: null,
  DATA_GLOBAL_VOL_AUTRE: null,
  DATA_SACS_VOL_TOTAL: null,
  DATA_SACS_VOL_PLASTIQUE: null,
  DATA_SACS_VOL_CAOUTCHOUC: null,
  DATA_SACS_VOL_BOIS: null,
  DATA_SACS_VOL_TEXTILE: null,
  DATA_SACS_VOL_PAPIER: null,
  DATA_SACS_VOL_METAL: null,
  DATA_SACS_VOL_VERRE: null,
  DATA_SACS_VOL_AUTRE: null,
  DATA_DV_VOL_TOTAL: null,
  DATA_DV_VOL_PLASTIQUE: null,
  DATA_DV_VOL_CAOUTCHOUC: null,
  DATA_DV_VOL_BOIS: null,
  DATA_DV_VOL_TEXTILE: null,
  DATA_DV_VOL_PAPIER: null,
  DATA_DV_VOL_METAL: null,
  DATA_DV_VOL_VERRE: null,
  DATA_DV_VOL_AUTRE: null,
  DATA_SACS_POIDS_TOTAL: null,
  DATA_SACS_POIDS_PLASTIQUE: null,
  DATA_SACS_POIDS_CAOUTCHOUC: null,
  DATA_SACS_POIDS_BOIS: null,
  DATA_SACS_POIDS_TEXTILE: null,
  DATA_SACS_POIDS_PAPIER: null,
  DATA_SACS_POIDS_METAL: null,
  DATA_SACS_POIDS_VERRE: null,
  DATA_SACS_POIDS_AUTRE: null,
  DATA_DV_POIDS: null,
  DATA_DECHETS_INDICATEURS: [],
  DATA_DECHETS_AUTRES: [],
  DATA_DECHETS_MARQUES: [],
  MARQUES_EXHAUSTIVITE: null,
  COMMENTAIRE_DV: "",
  COMMENTAIRE_GENERAL: "",
};

/**
 *
 */
export function buildReleveJsonXLS(releveData) {
  let r = JSON.parse(JSON.stringify(releveJsonXLS));
  r.STRUCTURE_NOM = releveData.structure?.nom;
  r.STRUCTURE_ID = releveData.structure?.id;
  r.EV_NOM = releveData.evtName;
  r.EV_DESCR = releveData.structure?.nom + " - " + releveData.evtDate;
  r.EV_DATE_DEBUT = releveData.evtDate;
  r.EV_DATE_FIN = releveData.evtDate;
  r.EV_POINT_GPS_LAT = releveData.centroid.latitude;
  r.EV_POINT_GPS_LON = releveData.centroid.longitude;
  r.EV_NIVEAU = releveData.pickupLevel;
  r.EV_ENVERGURE_ID = releveData.projetEnvergure;
  // r.EV_INITIATIVES_IDS = TODO;
  r.EV_SPOT_NOM = releveData.adopt1spot ? "à remplir" : null;
  r.EV_SPOT_ID = releveData.adopt1spot ? "à remplir" : null;
  r.CONTEXTE_TYPE_MILIEU_UUID = releveData.typeMilieu;
  r.CONTEXTE_TYPE_LIEU_UUID = releveData.typeLieu;
  r.CONTEXTE_TYPE_DECHET_UUID = releveData.typeDechet;
  r.CONTEXTE_NOM_ZONE = releveData.zoneName;
  r.CONTEXTE_DUREE = releveData.dureeH;
  r.CONTEXTE_PARTICIPANT_ORGA = releveData.nbParticipants;
  r.CONTEXTE_PARTICIPANT_EXT = releveData.nbPartExt;
  r.CONTEXTE_GEOMETRIE = buildContextGeo(
    releveData.polygonCoords,
    releveData.linearCoords
  );
  // VolumesBags
  r.DATA_SACS_VOL_TOTAL = parseFloat(releveData.vBags_TOTAL); // champ obligatoire
  r.DATA_SACS_VOL_PLASTIQUE = releveData.vBags_Plastique
    ? parseFloat(releveData.vBags_Plastique)
    : 0;
  r.DATA_SACS_VOL_CAOUTCHOUC = releveData.vBags_Caoutchouc
    ? parseFloat(releveData.vBags_Caoutchouc)
    : 0;
  r.DATA_SACS_VOL_BOIS = releveData.vBags_Bois
    ? parseFloat(releveData.vBags_Bois)
    : 0;
  r.DATA_SACS_VOL_TEXTILE = releveData.vBags_Textile
    ? parseFloat(releveData.vBags_Textile)
    : 0;
  r.DATA_SACS_VOL_PAPIER = releveData.vBags_Carton
    ? parseFloat(releveData.vBags_Carton)
    : 0;
  r.DATA_SACS_VOL_METAL = releveData.vBags_Metal
    ? parseFloat(releveData.vBags_Metal)
    : 0;
  r.DATA_SACS_VOL_VERRE = releveData.vBags_Verre
    ? parseFloat(releveData.vBags_Verre)
    : 0;
  r.DATA_SACS_VOL_AUTRE = releveData.vBags_Unknown
    ? parseFloat(releveData.vBags_Unknown)
    : 0;
  // VolumeMainWastes - DV pour Dechets VOlumineux
  r.DATA_DV_VOL_TOTAL = releveData.vMain_TOTAL
    ? parseFloat(releveData.vMain_TOTAL)
    : 0;
  r.DATA_DV_VOL_PLASTIQUE = releveData.vMain_Plastique
    ? parseFloat(releveData.vMain_Plastique)
    : 0;
  r.DATA_DV_VOL_CAOUTCHOUC = releveData.vMain_Caoutchouc
    ? parseFloat(releveData.vMain_Caoutchouc)
    : 0;
  r.DATA_DV_VOL_BOIS = releveData.vMain_Bois
    ? parseFloat(releveData.vMain_Bois)
    : 0;
  r.DATA_DV_VOL_TEXTILE = releveData.vMain_Textile
    ? parseFloat(releveData.vMain_Textile)
    : 0;
  r.DATA_DV_VOL_PAPIER = releveData.vMain_Carton
    ? parseFloat(releveData.vMain_Carton)
    : 0;
  r.DATA_DV_VOL_METAL = releveData.vMain_Metal
    ? parseFloat(releveData.vMain_Metal)
    : 0;
  r.DATA_DV_VOL_VERRE = releveData.vMain_Verre
    ? parseFloat(releveData.vMain_Verre)
    : 0;
  r.DATA_DV_VOL_AUTRE = releveData.vMain_Unknown
    ? parseFloat(releveData.vMain_Unknown)
    : 0;
  // Volumes globaux
  r.DATA_GLOBAL_VOL_TOTAL = r.DATA_SACS_VOL_TOTAL + r.DATA_DV_VOL_TOTAL;
  r.DATA_GLOBAL_VOL_PLASTIQUE =
    r.DATA_SACS_VOL_PLASTIQUE + r.DATA_DV_VOL_PLASTIQUE;
  r.DATA_GLOBAL_VOL_CAOUTCHOUC =
    r.DATA_SACS_VOL_CAOUTCHOUC + r.DATA_DV_VOL_CAOUTCHOUC;
  r.DATA_GLOBAL_VOL_BOIS = r.DATA_SACS_VOL_BOIS + r.DATA_DV_VOL_BOIS;
  r.DATA_GLOBAL_VOL_TEXTILE = r.DATA_SACS_VOL_TEXTILE + r.DATA_DV_VOL_TEXTILE;
  r.DATA_GLOBAL_VOL_PAPIER = r.DATA_SACS_VOL_PAPIER + r.DATA_DV_VOL_PAPIER;
  r.DATA_GLOBAL_VOL_METAL = r.DATA_SACS_VOL_METAL + r.DATA_DV_VOL_METAL;
  r.DATA_GLOBAL_VOL_VERRE = r.DATA_SACS_VOL_VERRE + r.DATA_DV_VOL_VERRE;
  r.DATA_GLOBAL_VOL_AUTRE = r.DATA_SACS_VOL_AUTRE + r.DATA_DV_VOL_AUTRE;
  // weightBags
  r.DATA_SACS_POIDS_TOTAL = releveData.wBags_TOTAL; // champ obligatoire
  r.DATA_SACS_POIDS_PLASTIQUE = releveData.wBags_Plastique
    ? releveData.wBags_Plastique
    : 0;
  r.DATA_SACS_POIDS_CAOUTCHOUC = releveData.wBags_Caoutchouc
    ? releveData.wBags_Caoutchouc
    : 0;
  r.DATA_SACS_POIDS_BOIS = releveData.wBags_Bois ? releveData.wBags_Bois : 0;
  r.DATA_SACS_POIDS_TEXTILE = releveData.wBags_Textile
    ? releveData.wBags_Textile
    : 0;
  r.DATA_SACS_POIDS_PAPIER = releveData.wBags_Carton
    ? releveData.wBags_Carton
    : 0;
  r.DATA_SACS_POIDS_METAL = releveData.wBags_Metal ? releveData.wBags_Metal : 0;
  r.DATA_SACS_POIDS_VERRE = releveData.wBags_Verre ? releveData.wBags_Verre : 0;
  r.DATA_SACS_POIDS_AUTRE = releveData.wBags_Unknown
    ? releveData.wBags_Unknown
    : 0;
  r.DATA_DV_POIDS = releveData.wMain_TOTAL ? releveData.wMain_TOTAL : 0;
  r.DATA_DECHETS_INDICATEURS = extractdataDechetsIndicateurs(
    releveData.dataDechetsIndicateurs
  );
  r.DATA_DECHETS_AUTRES = releveData.dataDechetsAutres
    .map((waste) => waste.dcsmm + ":" + waste.number)
    .join(",");
  //  comptage des dechets
  r.DATA_DECHETS_MARQUES = releveData.dataDechetsMarques
    .map((marque) => marque.name + ":" + marque.number)
    .join(",");
  r.MARQUES_EXHAUSTIVITE = releveData.exhaustiviteMarques
    ? releveData.exhaustiviteMarques
    : 0;
  //Comments
  r.COMMENTAIRE_DV = releveData.DV_comments;
  r.COMMENTAIRE_GENERAL = releveData.commentaire;
  console.log("buildReleveJsonXLS=" + JSON.stringify(r));
  return r;
}

/**
 *
 * @param polygonCoords  exemple de format d entrée  {polygonCoords"= [{"latitude"= 43.645123449656985, "longitude"= 6.917703375220299},
 * {"latitude"= 43.652774080689916, "longitude"= 6.9160789623856544},
 * {"latitude"= 43.65325221277289, "longitude"= 6.931030265986919},
 *  {"latitude"= 43.64430655815469, "longitude"= 6.931112743914127}]}
 */
function buildContextGeo(polygonCoords, linearCoords) {
  let ctxGeo = {
    type: "FeatureCollection",
    features: [],
  };

  // Add Polygon feature if polygonCoords are provided
  if (polygonCoords && polygonCoords.length > 0) {
    ctxGeo.features.push({
      type: "Feature",
      properties: [],
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            ...polygonCoords.map((polygonCoord) => {
              return [polygonCoord.longitude, polygonCoord.latitude];
            }),
            [polygonCoords[0].longitude, polygonCoords[0].latitude],
          ],
        ],
      },
    });
  }

  // Add LineString feature if linearCoords are provided
  if (linearCoords && linearCoords.length > 0) {
    ctxGeo.features.push({
      type: "Feature",
      properties: [],
      geometry: {
        type: "LineString",
        coordinates: linearCoords.map((linearCoord) => {
          return [linearCoord.longitude, linearCoord.latitude];
        }),
      },
    });
  }
  //console.log("ctxGeo:", JSON.stringify(ctxGeo));
  return JSON.stringify(ctxGeo);
}

export const useThousandSeparator = (number) => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

// Helper function to round number to specified decimal places
export const roundToDecimalPlaces = (num, decimalPlaces) => {
  const factor = Math.pow(10, decimalPlaces);
  return Math.round(num * factor) / factor;
};

function extractdataDechetsIndicateurs(jsonObj) {
  if (typeof jsonObj !== "object" || jsonObj === null) {
    throw new Error("Input must be a non-null JSON object.");
  }

  return Object.entries(jsonObj)
    .map(([key, value]) => `${key}:${value}`)
    .join(",");
}
