export const TYPE_MILIEU = {
  "0e6c0232-dd31-4ad2-9617-d13485480ef9": "Cours d'eau",
  "15f80e5b-6e91-416d-a509-6c98f0aa29cf": "Lac et Marais",
  "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d": "Lagune et étang côtier",
  "08b669e5-c807-40d3-806e-5d3191c09dfc": "Littoral (terrestre)",
  "014539f4-b8b0-4a9c-a209-993a13a147aa": "Mer - Océan",
  "2985e147-53eb-48ff-aea9-9f2126d0ce83": "Montagne",
  "2c7b9fe3-b729-4577-b128-790a48d4d890": "Multi-lieux",
  "232285f1-a36c-4c20-8cd6-00ad21fc5c37":
    "Zone naturelle ou rurale (hors littoral et montagne)",
  "167b5f65-221c-40c5-afc2-d3e397738691": "Zone urbaine",
};

export const TYPE_LIEU = {
  "f5d03de6-f3fb-4595-bd68-071daf453953": "Autre espace naturel",
  "bb845e39-5543-46db-b835-b42c616ad62e": "Berge naturelle (hors plage)",
  "9f98ff48-e509-4d59-976b-98ddec122c6b": "Canaux et Salins",
  "f048d0df-1271-4bb2-b21a-c6d58024e4ad":
    "Champ - prairie - lande - garrigue - maquis",
  "fc91294b-445b-4610-addd-1884f7b4c3ca": "Champ et prairie",
  "56ef88d1-a127-4312-b4fc-e97804a756b3": "Digue et ouvrage",
  "5a5b9691-71eb-4b85-911d-a4f70576b5ac": "Dune",
  "6f482aa4-fc6e-4efc-9955-3eb14d881775":
    "Espace naturel en arrière plage/côte (calanques, colline …)",
  "df6eb7ca-bc7a-42b3-9bb1-fe94c619b27f": "Forêt",
  "af5b532d-e85f-41c3-8370-a36bc53e4d51":
    "Lit du cours d'eau (fond et/ou surface)",
  "6ce08387-ba56-4147-b2e0-7dcd7630bc1c": "Mangrove",
  "3061c0a1-abc6-474b-b219-2a55879ed482": "Mer - Océan",
  "8e047d6c-f1cb-4885-b223-60d7ee416c34": "Multi-lieux",
  "794a1ace-8ef3-4c28-b3cd-9ee8673b889e": "Parc urbain - Jardin public",
  "7c099b82-5a3a-42da-adc2-acc909023b93": "Parking",
  "f8c597eb-6a72-40f5-8a11-63ace1adb75f": "Piste de ski",
  "4214888a-8f13-428c-8371-23084b7fee3e": "Plage (sable, galets, gravillons)",
  "950570b8-1dd3-4a6e-97ef-87d94b501db7": "Plan d'eau (fond et/ou surface)",
  "459dd694-d67a-4a05-8f52-ea85a8195edc": "Port",
  "cb7517d1-e63f-499d-9016-8499cb5b6ae1": "Port - barrage",
  "c853cac4-2c1f-4f0f-8919-113ab122abe6": "Port - barrage - écluse",
  "bbb12717-2e41-440c-b45b-bc2d2f39321b": "Quai, Digue et ouvrage",
  "3be65b46-97af-485b-8bf3-cbef5e8fb4e9": "Rocher et crique",
  "791cc753-9ddb-4b51-838f-fec661ac8841": "Route",
  "d52ba08d-ef95-40aa-93df-ad8590a398f5": "Route, rue, place",
  "774d7a3a-bd7d-474e-a327-4e9bc0f8f352": "Sentier et chemin",
  "fb9de3f5-ff7e-4199-8a53-fb34d9f4ada6": "Station de ski",
  "cbcddc23-6c92-42e9-9a78-fa93e0e6f24d": "Voie de chemin de fer",
};

export const TYPE_DECHET = {
  "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf": "Echoué",
  "4b8c42a7-a4f2-4e25-a58e-0e007d0bc932": "Enfoui",
  "443c9f2d-da72-42ae-9ff5-8f2facd49b8f": "Flottant",
  "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f": "Flottant et Fond",
  "c5c9b6b7-8147-4dba-a11e-64c171ee033e": "Fond",
  "7c96c580-6985-48bf-b4de-f2cd41509a74": "Présent au sol (abandonné)",
  "c7381604-ee86-4dce-9476-1951b1efb391":
    "Présent au sol (abandonné) et échoué",
};

export const TAB_LIEUX_MILIEUX = [
  {
    // Mer - Océan | Mer - Océan
    id_milieu: "014539f4-b8b0-4a9c-a209-993a13a147aa",
    id_lieu: "3061c0a1-abc6-474b-b219-2a55879ed482",
    id_dechet: [
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Rocher et crique
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "3be65b46-97af-485b-8bf3-cbef5e8fb4e9",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Plage (sable, galets, gravillons)
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "4214888a-8f13-428c-8371-23084b7fee3e",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Port
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "459dd694-d67a-4a05-8f52-ea85a8195edc",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Digue et ouvrage
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "56ef88d1-a127-4312-b4fc-e97804a756b3",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Dune
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "5a5b9691-71eb-4b85-911d-a4f70576b5ac",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Littoral (terrestre)	| Mangrove
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "6ce08387-ba56-4147-b2e0-7dcd7630bc1c",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Littoral (terrestre)	| Espace naturel en arrière plage/côte (calanques, colline …)
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "6f482aa4-fc6e-4efc-9955-3eb14d881775",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Sentier et chemin
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "774d7a3a-bd7d-474e-a327-4e9bc0f8f352",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Route
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "791cc753-9ddb-4b51-838f-fec661ac8841",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Parc urbain - Jardin public
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "794a1ace-8ef3-4c28-b3cd-9ee8673b889e",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Parking
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "7c099b82-5a3a-42da-adc2-acc909023b93",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Littoral (terrestre) | Multi-lieux
    id_milieu: "08b669e5-c807-40d3-806e-5d3191c09dfc",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Plan d'eau (fond et/ou surface)
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "950570b8-1dd3-4a6e-97ef-87d94b501db7",
    id_dechet: [
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Rocher et crique
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "3be65b46-97af-485b-8bf3-cbef5e8fb4e9",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Plage (sable, galets, gravillons)
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "4214888a-8f13-428c-8371-23084b7fee3e",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Port
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "459dd694-d67a-4a05-8f52-ea85a8195edc",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Digue et ouvrage
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "56ef88d1-a127-4312-b4fc-e97804a756b3",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Espace naturel en arrière plage/côte (calanques, colline …)
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "6f482aa4-fc6e-4efc-9955-3eb14d881775",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Sentier et chemin
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "774d7a3a-bd7d-474e-a327-4e9bc0f8f352",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Route
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "791cc753-9ddb-4b51-838f-fec661ac8841",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Canaux et Salins
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "9f98ff48-e509-4d59-976b-98ddec122c6b",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Parc urbain - Jardin public
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "794a1ace-8ef3-4c28-b3cd-9ee8673b889e",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Parking
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "7c099b82-5a3a-42da-adc2-acc909023b93",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Lagune et étang côtier | Multi-lieux
    id_milieu: "0e3a1f9b-33b5-4c4b-8b52-d403cec6919d",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Cours d'eau | Lit du cours d'eau (fond et/ou surface)
    id_milieu: "0e6c0232-dd31-4ad2-9617-d13485480ef9",
    id_lieu: "af5b532d-e85f-41c3-8370-a36bc53e4d51",
    id_dechet: [
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Cours d'eau | Plage (sable, galets, gravillons)
    id_milieu: "0e6c0232-dd31-4ad2-9617-d13485480ef9",
    id_lieu: "4214888a-8f13-428c-8371-23084b7fee3e",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Cours d'eau | Berge naturelle (hors plage)
    id_milieu: "0e6c0232-dd31-4ad2-9617-d13485480ef9",
    id_lieu: "bb845e39-5543-46db-b835-b42c616ad62e",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Cours d'eau | Parc urbain - Jardin public
    id_milieu: "0e6c0232-dd31-4ad2-9617-d13485480ef9",
    id_lieu: "794a1ace-8ef3-4c28-b3cd-9ee8673b889e",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Cours d'eau | Quai, Digue et ouvrage
    id_milieu: "0e6c0232-dd31-4ad2-9617-d13485480ef9",
    id_lieu: "bbb12717-2e41-440c-b45b-bc2d2f39321b",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Cours d'eau | Port - barrage - écluse
    id_milieu: "0e6c0232-dd31-4ad2-9617-d13485480ef9",
    id_lieu: "c853cac4-2c1f-4f0f-8919-113ab122abe6",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Cours d'eau | Multi-lieux
    id_milieu: "0e6c0232-dd31-4ad2-9617-d13485480ef9",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lac et Marais | Plan d'eau (fond et/ou surface)
    id_milieu: "15f80e5b-6e91-416d-a509-6c98f0aa29cf",
    id_lieu: "950570b8-1dd3-4a6e-97ef-87d94b501db7",
    id_dechet: [
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Lac et Marais | Plage (sable, galets, gravillons)
    id_milieu: "15f80e5b-6e91-416d-a509-6c98f0aa29cf",
    id_lieu: "4214888a-8f13-428c-8371-23084b7fee3e",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lac et Marais | Berge naturelle (hors plage)
    id_milieu: "15f80e5b-6e91-416d-a509-6c98f0aa29cf",
    id_lieu: "bb845e39-5543-46db-b835-b42c616ad62e",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lac et Marais | Quai, Digue et ouvrage
    id_milieu: "15f80e5b-6e91-416d-a509-6c98f0aa29cf",
    id_lieu: "bbb12717-2e41-440c-b45b-bc2d2f39321b",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Lac et Marais | Port - barrage
    id_milieu: "15f80e5b-6e91-416d-a509-6c98f0aa29cf",
    id_lieu: "cb7517d1-e63f-499d-9016-8499cb5b6ae1",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: false,
    area_data: true,
  },
  {
    // Lac et Marais | Multi-lieux
    id_milieu: "15f80e5b-6e91-416d-a509-6c98f0aa29cf",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Zone urbaine | Voie de chemin de fer
    id_milieu: "167b5f65-221c-40c5-afc2-d3e397738691",
    id_lieu: "cbcddc23-6c92-42e9-9a78-fa93e0e6f24d",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Zone urbaine | Route, rue, place
    id_milieu: "167b5f65-221c-40c5-afc2-d3e397738691",
    id_lieu: "d52ba08d-ef95-40aa-93df-ad8590a398f5",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Zone urbaine | Parking
    id_milieu: "167b5f65-221c-40c5-afc2-d3e397738691",
    id_lieu: "7c099b82-5a3a-42da-adc2-acc909023b93",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Zone urbaine | Parc urbain - jardin public
    id_milieu: "167b5f65-221c-40c5-afc2-d3e397738691",
    id_lieu: "794a1ace-8ef3-4c28-b3cd-9ee8673b889e",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Zone urbaine | Multi-lieux
    id_milieu: "167b5f65-221c-40c5-afc2-d3e397738691",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Voie de chemin de fer
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "cbcddc23-6c92-42e9-9a78-fa93e0e6f24d",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Route
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "791cc753-9ddb-4b51-838f-fec661ac8841",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Parking
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "7c099b82-5a3a-42da-adc2-acc909023b93",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Forêt
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "df6eb7ca-bc7a-42b3-9bb1-fe94c619b27f",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Champ - prairie - lande - garrigue - maquis
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "f048d0df-1271-4bb2-b21a-c6d58024e4ad",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Sentier et chemin
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "774d7a3a-bd7d-474e-a327-4e9bc0f8f352",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Autre espace naturel
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "f5d03de6-f3fb-4595-bd68-071daf453953",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Zone naturelle ou rurale (hors littoral et montagne) | Multi-lieux
    id_milieu: "232285f1-a36c-4c20-8cd6-00ad21fc5c37",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Montagne | Piste de ski
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "f8c597eb-6a72-40f5-8a11-63ace1adb75f",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Montagne | Station de ski
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "fb9de3f5-ff7e-4199-8a53-fb34d9f4ada6",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Montagne | Parking
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "7c099b82-5a3a-42da-adc2-acc909023b93",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Montagne | Route
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "791cc753-9ddb-4b51-838f-fec661ac8841",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Montagne | Forêt
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "df6eb7ca-bc7a-42b3-9bb1-fe94c619b27f",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Montagne | Champ et prairie
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "fc91294b-445b-4610-addd-1884f7b4c3ca",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Montagne | Sentier et chemin
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "774d7a3a-bd7d-474e-a327-4e9bc0f8f352",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: true,
    area_data: true,
  },
  {
    // Montagne | Autre espace naturel
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "f5d03de6-f3fb-4595-bd68-071daf453953",
    id_dechet: ["7c96c580-6985-48bf-b4de-f2cd41509a74"],
    linear_data: false,
    area_data: true,
  },
  {
    // Montagne | Multi-lieux
    id_milieu: "2985e147-53eb-48ff-aea9-9f2126d0ce83",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
  {
    // Multi-lieux | Multi-lieux
    id_milieu: "2c7b9fe3-b729-4577-b128-790a48d4d890",
    id_lieu: "8e047d6c-f1cb-4885-b223-60d7ee416c34",
    id_dechet: [
      "7c96c580-6985-48bf-b4de-f2cd41509a74",
      "d5badf12-8fbb-4daa-92c4-f9d1f0c667cf",
      "c7381604-ee86-4dce-9476-1951b1efb391",
      "c5c9b6b7-8147-4dba-a11e-64c171ee033e",
      "443c9f2d-da72-42ae-9ff5-8f2facd49b8f",
      "8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f",
    ],
    linear_data: true,
    area_data: true,
  },
];
