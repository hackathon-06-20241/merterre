export const DECHETS_AUTRES = [
  {
    dcsmm: "remed_dechets_DCSMM_Allumettes",
    name: "Allumettes et feux d'artifice",
  },
  { dcsmm: "remed_dechets_DCSMM_Ampoules", name: "Ampoules" },
  { dcsmm: "remed_dechets_DCSMM_Neons", name: "Néons" },
  {
    dcsmm: "remed_dechets_DCSMM_Anneaux_joints_et_opercules",
    name: "Anneaux et opercules de bouchons",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Autres_objets_lies_a_la_peche",
    name: "Autres objets liés à la pêche",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Batonnets_de_peche_lumineux",
    name: "Batônnets de pêche lumineux",
  },
  { dcsmm: "remed_dechets_DCSMM_Bobines_de_peche", name: "Bobines de pêche" },
  {
    dcsmm: "remed_dechets_DCSMM_Caisses_a_poisson_en_plastique",
    name: "Caisses à poisson en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Caisses_a_poisson_en_polystyrene",
    name: "Caisses à poisson en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Caisses_a_poissons",
    name: "Caisses à poissons",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Casiers_de_peche_bulots",
    name: "Casiers de pêche bulots",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Casiers_de_peche_crabes",
    name: "Casiers de pêche crabes",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Casiers_de_peche_en_bois",
    name: "Casiers de pêche en bois",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Casiers_de_peche_en_metal",
    name: "Casiers de pêche en métal",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Filaments_issus_de_perruques_de_chalut",
    name: "Filaments issus de perruques de chalut",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Flotteurs_boules_en_verre_pour_filet",
    name: "Flotteurs boules en verre pour filet",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Flotteurs_externes_pour_filet",
    name: "Flotteurs externes pour filet",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Flotteurs_internes_pour_filet",
    name: "Flotteurs internes pour filet",
  },
  { dcsmm: "remed_dechets_DCSMM_Leurres_souples", name: "Leurres souples" },
  {
    dcsmm: "remed_dechets_DCSMM_Marquages_peche",
    name: "Marquages et tags (pêche)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Protections_hamecon",
    name: "Mousse de protections hameçon",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Perruques_chalut_de_fond",
    name: "Perruques chalut de fond",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Pots_a_pieuvre_ceramique",
    name: "Pots à pieuvre céramique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Pots_a_pieuvre_plastique",
    name: "Pots à poulpe plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Paquets_de_cigarettes_et_emballages",
    name: "Emballages de paquets de cigarette",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Embouts_de_cigarette",
    name: "Embouts de cigarette",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Autres_objets_d_hygiene_et_soins_personnels",
    name: "Autres objets d'hygiène et soins personnels",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Bouchons_d_oreille",
    name: "Bouchons d'oreille",
  },
  { dcsmm: "remed_dechets_DCSMM_Brosses_a_dents", name: "Brosses à dents" },
  {
    dcsmm: "remed_dechets_DCSMM_Blocs_WC",
    name: "Desodorisants toilette (dont Blocs WC)",
  },
  { dcsmm: "remed_dechets_DCSMM_Preservatifs", name: "Préservatifs" },
  { dcsmm: "remed_dechets_DCSMM_Rasoirs", name: "Rasoirs" },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_bois_inferieurs_a_50_cm",
    name: "Autres objets en bois inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_bois",
    name: "Autres objets en bois supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_metal_inferieurs_a_50_cm",
    name: "Autres objets en métal inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_metal_superieurs_a_50_cm",
    name: "Autres objets en métal supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_papier_carton",
    name: "Autres objets en papier-carton",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_plastique",
    name: "Autres objets en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_polystyrene",
    name: "Autres objets en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_ceramique",
    name: "Autres objets céramique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_verre",
    name: "Autres objets verre",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_textiles",
    name: "Autres objets textile",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_textile",
    name: "Fragments de textile",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_en_caoutchouc",
    name: "Autres objets en caoutchouc",
  },
  { dcsmm: "remed_dechets_DCSMM_Chambres_a_air", name: "Chambres à air" },
  {
    dcsmm: "remed_dechets_DCSMM_Elastiques_de_bureau",
    name: "Elastiques de bureau",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_non_identifies_en_caoutchouc",
    name: "Fragments non identifiés en caoutchouc",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_objets_sanitaires",
    name: "Autres objets sanitaires",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_dechets_medicaux",
    name: "Autres objets médicaux",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Gants_a_usage_unique",
    name: "Gants à usage unique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Manches_a_balais",
    name: "Balais et outils inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Manches_a_balais_superieurs_a_50_cm",
    name: "Balais et outils supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Ballons_de_baudruche",
    name: "Ballons de baudruche",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_banderoles_de_signalement",
    name: "Banderoles de signalement",
  },
  { dcsmm: "remed_dechets_DCSMM_Barbecues", name: "Barbecues jetables" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_batons_de_ski_ou_morceaux",
    name: "Bâtons de ski (ou morceaux)",
  },
  { dcsmm: "remed_dechets_DCSMM_Batons_de_sucette", name: "Bâtons de sucette" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Batteries_de_vehicules",
    name: "Batteries de vehicules",
  },
  { dcsmm: "remed_dechets_DCSMM_Billes_de_bois", name: "Billes de bois" },
  { dcsmm: "remed_dechets_DCSMM_Bocaux_et_pots", name: "Bocaux et pots" },
  {
    dcsmm: "remed_dechets_DCSMM_Bois_alimentaire",
    name: "Bâtonnets glaces, couverts, cure-dents",
  },
  { dcsmm: "remed_dechets_DCSMM_Boites_d_appats", name: "Boîtes d'appâts" },
  { dcsmm: "remed_dechets_DCSMM_Tetrapacks_appats", name: "Tetrapacks appâts" },
  {
    dcsmm: "remed_dechets_DCSMM_Boites_de_conserve",
    name: "Boîtes de conserve",
  },
  { dcsmm: "remed_dechets_DCSMM_Bidons_divers", name: "Bidons divers" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Boites_en_metal_inferieur_a_50_cm",
    name: "Boites en métal inférieures à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Boites_en_metal_superieur_a_50_cm",
    name: "Boites en métal supérieures à 50 cm",
  },
  { dcsmm: "remed_dechets_DCSMM_Bouteilles_de_gaz", name: "Bouteilles de gaz" },
  { dcsmm: "remed_dechets_DCSMM_Fûts", name: "Fûts et barils" },
  { dcsmm: "remed_dechets_DCSMM_Bombes_aerosol", name: "Bombes aérosol" },
  { dcsmm: "remed_dechets_DCSMM_Bouchons_de_liege", name: "Bouchons de liège" },
  {
    dcsmm: "remed_dechets_DCSMM_Bouchons_autres",
    name: "Bouchons et couvercles autres",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Bouchons_de_bouteille",
    name: "Bouchons et couvercles de bouteille",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Bouchons_non_alimentaire",
    name: "Bouchons et couvercles non alimentaire",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Bouees_en_plastique",
    name: "Autres bouées en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Bouees_en_polystyrene",
    name: "Flotteurs et bouées en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Bouteilles_en_verre",
    name: "Bouteilles en verre",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_contenants_en_plastique",
    name: "Autres contenants en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_contenants_en_polystyrene",
    name: "Autres contenants en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Bouteilles_sproduit_de_nettoyage",
    name: "Bouteilles et contenants produit de nettoyage",
  },
  {
    dcsmm:
      "remed_top_34_dechets_DCSMM_thesaurus_Bouteilles_plastique_non_alimentaires",
    name: "Bouteilles plastique non alimentaires",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Caisses_et_paniers",
    name: "Caisses et paniers",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Contenants_huile_de_moteur_inferieurs_a_50_cm",
    name: "Contenants huile de moteur inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Contenants_huile_de_moteur_superieurs_a_50_cm",
    name: "Contenants huile de moteur supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Contenants_cosmetique",
    name: "Contenants hygiène et soins corporels",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Cremes_solaire",
    name: "Crèmes solaire et produits de plage",
  },
  { dcsmm: "remed_dechets_DCSMM_Jerrycans", name: "Jerrycans" },
  { dcsmm: "remed_dechets_DCSMM_Briquets", name: "Briquets" },
  {
    dcsmm: "remed_dechets_DCSMM_Cageots",
    name: "Caisses en bois, boites, paniers",
  },
  { dcsmm: "remed_dechets_DCSMM_Canettes", name: "Canettes en métal" },
  { dcsmm: "remed_dechets_DCSMM_Cartons", name: "Cartons" },
  {
    dcsmm: "remed_dechets_DCSMM_Cartouches",
    name: "Cartouches et bourre de chasse",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Casques_de_chantier",
    name: "Casques de protection",
  },
  {
    dcsmm: "remed_dechets_DCSMM_CDs_et_boites",
    name: "CD et DVD (dont boîtes)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_chaufferettes",
    name: "Chaufferettes",
  },
  { dcsmm: "remed_dechets_DCSMM_Bottes", name: "Bottes" },
  { dcsmm: "remed_dechets_DCSMM_Chaussures", name: "Chaussures (hors tongs)" },
  {
    dcsmm: "remed_dechets_DCSMM_Chaussures_cuir_tissu",
    name: "Chaussures et sandales",
  },
  { dcsmm: "remed_dechets_DCSMM_Tongs", name: "Tongs" },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_cigarettes_electroniques_et_recharges",
    name: "Cigarettes électroniques et recharges",
  },
  { dcsmm: "remed_dechets_DCSMM_Bougies", name: "Bougies" },
  {
    dcsmm: "remed_dechets_DCSMM_Morceaux_de_cire_inferieurs_a_10_cm",
    name: "Paraffine inférieure à 10 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Morceaux_de_cire_superieurs_a_10_cm",
    name: "Paraffine supérieure à 10 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Compresses_et_pansements",
    name: "Compresses et pansements",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Emballages_alimentaire_en_polystyrene",
    name: "Contenants alimentaire en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Emballages_alimentaire_en_plastique",
    name: "Contenants alimentaires en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_cordages_de_securite",
    name: "Cordages de sécurité",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Cordages_et_ficelles_inferieures_a_1_cm",
    name: "Cordages et ficelles inférieures à 1 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Cordages_et_ficelles_inferieures_a_1_cm_autres",
    name: "Cordages et ficelles inférieures à 1 cm (autres)",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Cordages_et_ficelles_inferieures_a_1_cm_peche",
    name: "Cordages et ficelles inférieures à 1 cm (pêche)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Cordages_et_ficelles_superieures_a_1cm",
    name: "Cordages et ficelles supérieures à 1 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Cordes",
    name: "Cordes, ficelles et filets textile",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Filets_et_cordages_emmeles_tout_type",
    name: "Filets et cordages emmêlés (tout type)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Filets_et_cordages_emmeles",
    name: "Filets et cordages emmêlés",
  },
  { dcsmm: "remed_dechets_DCSMM_Cotons_tiges", name: "Cotons-tiges" },
  {
    dcsmm: "remed_dechets_DCSMM_Cotons_tiges_papier",
    name: "Cotons-tiges (papier)",
  },
  { dcsmm: "remed_dechets_DCSMM_Adhesifs", name: "Adhésifs" },
  {
    dcsmm: "remed_dechets_DCSMM_Cercles_d_emballage",
    name: "Cercles d'emballage",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Baches",
    name: "Emballages industriels (Bâches, papier bulle,…)",
  },
  { dcsmm: "remed_dechets_DCSMM_Geotextiles", name: "Géotextiles" },
  {
    dcsmm: "remed_dechets_DCSMM_Protections_en_polystyrene",
    name: "Protections en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Rembourrages_en_polystyrene",
    name: "Rembourrages en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Autres_objets_de_l_agriculture",
    name: "Autres objets de l'agriculture",
  },
  { dcsmm: "remed_dechets_DCSMM_Baches_agricoles", name: "Bâches agricoles" },
  {
    dcsmm: "remed_dechets_DCSMM_Liens_de_vigne_plastique",
    name: "Liens de vigne plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Lien_de_vigne_textile",
    name: "Liens de vigne textile",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Plateaux_pour_semis_en_polystyrene",
    name: "Plateaux pour semis en polystyrène",
  },
  { dcsmm: "remed_dechets_DCSMM_Pots_de_fleur", name: "Pots de fleur" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Tuyaux_d_irrigation",
    name: "Tuyaux d'irrigation",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Collecteurs_a_naissains",
    name: "Collecteurs à naissains",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Elastiques_de_conchyliculture",
    name: "Elastiques de conchyliculture",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Objets_mytiliculture_hors_tahitiennes",
    name: "Objets mytiliculture (hors tahitiennes)",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Objets_ostreiculture_hors_collecteurs_a_naissains",
    name: "Objets ostreiculture (hors collecteurs à naissains)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Poches_de_conchyliculture",
    name: "Poches de conchyliculture",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Tahitiennes_conchylliculture",
    name: "Tahitiennes (conchyliculture)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Plastiques_plat_de_construction",
    name: "Déchet de la construction (chutes PVC, croisillons...)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Tubes_et_tuyaux_en_PVC",
    name: "Déchets de la construction (Tubes, tuyaux,…)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_dosettes_de_sucre_en_papier",
    name: "Dosettes de sucre en papier",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Emballages_carton",
    name: "Emballages alimentaires",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Emballages_alimentaires_autres",
    name: "Emballages alimentaires autres",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_emballages_alimentaires_plastique",
    name: "Emballages alimentaires plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_emballages_de_paille",
    name: "Emballages de paille",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Emballages_sucrerie_et_chips",
    name: "Emballages sucreries et chips",
  },
  { dcsmm: "remed_dechets_DCSMM_Emballages_fin", name: "Emballages fin" },
  {
    dcsmm: "remed_dechets_DCSMM_Emballages_medicaments",
    name: "Contenants et emballages médicaux",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Emballages_non_alimentaires_identifies",
    name: "Emballages non-alimentaires identifiés",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Etiquettes_de_bouteille",
    name: "Etiquettes de bouteille",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Autres_etiquettes",
    name: "Etiquettes diverses (hors bouteille)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Ferailles_indutrielles",
    name: "Ferrailles industrielles",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_resine",
    name: "Objets et fragments en fibre de verre",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Filets_inferieurs_a_50cm",
    name: "Filets inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Filets_superieurs_a_50cm",
    name: "Filets supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fils_et_bas_de_ligne",
    name: "Lignes et fils de pêche",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Cables_inferieurs_a_50_cm",
    name: "Câbles inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Cables_superieurs_a_50_cm",
    name: "Câbles supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Grillages_et_barbeles",
    name: "Fils de fer, grillages et barbelés",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fleurs_en_plastique",
    name: "Fleurs en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_forfaits_de_ski",
    name: "Forfaits de ski",
  },
  { dcsmm: "remed_dechets_DCSMM_Batons_de_colle", name: "Bâtons de colle" },
  {
    dcsmm: "remed_dechets_DCSMM_Stylos_et_crayons",
    name: "Stylos et bouchons",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_bois_inferieurs_a_50_cm",
    name: "Fragments de bois inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_en_bois_superieurs_a_50_cm",
    name: "Fragments en bois supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_metal_inferieurs_a_50_cm",
    name: "Fragments de métal inférieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_metal_superieurs_a_50_cm",
    name: "Fragments de métal supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_ceramique_superieurs_a_2_5_cm",
    name: "Fragments céramique supérieurs à 2,5 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_verre_superieurs_a_2_5_cm",
    name: "Fragments verre supérieurs à 2,5 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Fragments_de_film_plastique_0_2_5_cm",
    name: "Fragments de film plastique 0 - 2,5 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Fragments_de_film_plastique_2_5_50_cm",
    name: "Fragments de film plastique 2,5 - 50 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Fragments_de_film_plastique_superieurs_a_50_cm",
    name: "Fragments de film plastique supérieurs à 50 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_fragments_de_films_plastique_toute_taille",
    name: "Fragments de films plastique (toute taille)",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_fragments_de_plastique_tout_type_toute_taille",
    name: "Fragments de plastique (tout type, toute taille)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_plastique_0_2_5_cm",
    name: "Fragments de plastique 0 - 2,5 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_plastique_2_5_50_cm",
    name: "Fragments de plastique 2,5 - 50 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_fragments_de_plastique_dur_toute_taille",
    name: "Fragments de plastique dur (toute taille)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Fragments_de_plastique_dur_0_2_5_cm",
    name: "Fragments de plastique dur 0 - 2,5 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Fragments_de_plastique_dur_2_5_50_cm",
    name: "Fragments de plastique dur 2,5 - 50 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Fragments_de_plastique_dur_superieurs_a_50_cm",
    name: "Fragments de plastique dur supérieurs à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Fragments_de_plastique_superieurs_a_50_cm",
    name: "Fragments de plastique supérieurs à 50 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_fragments_de_polystyrene_toute_taille",
    name: "Fragments de polystyrène (toute taille)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Fragments_de_polystyrene_0_2_5_cm",
    name: "Fragments de polystyrène 0 - 2,5 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Fragments_de_polystyrene_2_5_50_cm",
    name: "Fragments de polystyrène 2,5 - 50 cm",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Fragments_de_polystyrene_superieurs_a_50_cm",
    name: "Fragments de polystyrène supérieurs à 50 cm",
  },
  { dcsmm: "remed_dechets_DCSMM_Feux_d_artifice", name: "Feux d'artifice" },
  {
    dcsmm: "remed_dechets_DCSMM_Fusees_de_detresse",
    name: "Fusées de détresse",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Reste_de_feux_d_artifice_en_plastique",
    name: "Restes de feux d'artifice en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Gobelet_en_carton",
    name: "Gobelets en carton",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Gobelets_en_plastique",
    name: "Gobelets en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Gobelets_en_polystyrene",
    name: "Gobelets en polystyrène",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_granules_plastiques_industriels_gpi_larmes_de_sirene",
    name: "Granulés Plastiques Industriels (GPI, Larmes de sirène)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_jalons_de_piste",
    name: "Jalons de piste",
  },
  { dcsmm: "remed_dechets_DCSMM_Joints", name: "Joints en rondelle" },
  {
    dcsmm: "remed_dechets_DCSMM_Ballons_en_plastique",
    name: "Balles et ballons en caoutchouc",
  },
  { dcsmm: "remed_dechets_DCSMM_Ballons_en_nylon", name: "Ballons en nylon" },
  { dcsmm: "remed_dechets_DCSMM_Jeux_de_plage", name: "Jeux de plage" },
  { dcsmm: "remed_dechets_DCSMM_Jouets", name: "Jouets" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_jouets_tout_type",
    name: "Jouets (tout type)",
  },
  { dcsmm: "remed_dechets_DCSMM_Journeaux", name: "Journaux et magazines" },
  {
    dcsmm: "remed_dechets_DCSMM_Lingettes_jetables",
    name: "Lingettes jetables",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_masques_tout_type",
    name: "Masques (tout type)",
  },
  { dcsmm: "remed_dechets_DCSMM_Masques", name: "Masques à usage unique" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Masques_en_tissu",
    name: "Masques en tissu",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_masques_et_lunettes_de_ski",
    name: "Masques et lunettes de ski",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Materiaux_de_construction",
    name: "Matériaux de construction (verre, brique, ciment)",
  },
  { dcsmm: "remed_dechets_DCSMM_Parbattages", name: "Bouées Pare-battages" },
  { dcsmm: "remed_dechets_DCSMM_thesaurus_gaffes", name: "Gaffes" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_gilets_de_sauvetage",
    name: "Gilets de sauvetage",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Ceinture_en_caoutchouc",
    name: "Ceintures en caoutchouc",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Equipements_de_plonge",
    name: "Equipements de plongée",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_Materiel_de_sport_nautique_non_motorise_hors_palmes",
    name: "Matériels de sport nautique (non motorisés, hors palmes)",
  },
  { dcsmm: "remed_dechets_DCSMM_thesaurus_Palmes", name: "Palmes" },
  { dcsmm: "remed_dechets_DCSMM_Neoprenes", name: "Vetements Néoprène" },
  { dcsmm: "remed_dechets_DCSMM_Medias_filtrants", name: "Médias filtrants" },
  { dcsmm: "remed_dechets_DCSMM_Megots", name: "Mégots" },
  {
    dcsmm: "remed_dechets_DCSMM_Morceaux_de_papier",
    name: "Fragments de papier",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Mouchoirs",
    name: "Mouchoirs, Essuies-tout, papier toilette",
  },
  { dcsmm: "remed_dechets_DCSMM_Eponges", name: "Eponges" },
  {
    dcsmm: "remed_dechets_DCSMM_Mousses_souples",
    name: "Fragments et autres objets en mousse",
  },
  { dcsmm: "remed_dechets_DCSMM_Mousses_rigides", name: "Mousses isolantes" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_elastiques_pour_cheveux",
    name: "Elastiques pour cheveux",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Objets_beaute_peignes_brosses_lunettes",
    name: "Objets beauté (peignes, brosses, lunettes, élastiques cheveux)",
  },
  { dcsmm: "remed_dechets_DCSMM_Balles_en_eponge", name: "Balles en éponge" },
  {
    dcsmm: "remed_dechets_DCSMM_Cartouches_d_injection",
    name: "Cartouches d'injection",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Cones_de_circulation",
    name: "Cônes de circulation",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Feuille_de_revetement_en_caoutchouc",
    name: "Feuilles de revêtement en caoutchouc",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_gaines_et_cables_electriques",
    name: "Gaines et câbles électriques",
  },
  { dcsmm: "remed_dechets_DCSMM_Gants_menager", name: "Gants ménagers" },
  {
    dcsmm: "remed_dechets_DCSMM_Gants_professionnel",
    name: "Gants professionnels",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Materiels_de_bricolage",
    name: "Matériels entretien et bricolage",
  },
  { dcsmm: "remed_dechets_DCSMM_Pinceaux", name: "Pinceaux" },
  { dcsmm: "remed_dechets_DCSMM_Rubalises", name: "Rubalises" },
  { dcsmm: "remed_dechets_DCSMM_Seaux", name: "Seaux" },
  { dcsmm: "remed_dechets_DCSMM_Colliers_serre_cable", name: "Serre-cable" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_vis_clous_boulons_agrafes",
    name: "Vis, clous, boulons, agrafes",
  },
  { dcsmm: "remed_dechets_DCSMM_Pailles", name: "Pailles" },
  { dcsmm: "remed_dechets_DCSMM_Palettes", name: "Palettes" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_panneaux_de_signalisation",
    name: "Panneaux de signalisation",
  },
  { dcsmm: "remed_dechets_DCSMM_Papier_aluminium", name: "Papier aluminium" },
  {
    dcsmm: "remed_dechets_DCSMM_Paquets_de_cigarette",
    name: "Paquets de cigarette",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Pieces_d_automobile",
    name: "Pièces de véhicule",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Pieces_de_voiture_inferieures_a_50_cm",
    name: "Pièces de voiture inférieures à 50 cm",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Pieces_de_voiture_superieures_a_50_cm",
    name: "Pièces de voiture supérieures à 50 cm",
  },
  { dcsmm: "remed_dechets_DCSMM_Roues", name: "Roues" },
  { dcsmm: "remed_dechets_DCSMM_Piles", name: "Piles" },
  { dcsmm: "remed_dechets_DCSMM_Pinces_a_linge", name: "Pinces à linge" },
  { dcsmm: "remed_dechets_DCSMM_Planches", name: "Planches" },
  {
    dcsmm: "remed_dechets_DCSMM_Plombs_et_hamecons",
    name: "Plombs et hameçons",
  },
  { dcsmm: "remed_dechets_DCSMM_Pneus_et_courroies", name: "Pneus" },
  { dcsmm: "remed_dechets_DCSMM_Pots_de_peinture", name: "Pots de peinture" },
  { dcsmm: "remed_dechets_DCSMM_Autres_polluants", name: "Autres polluants" },
  { dcsmm: "remed_dechets_DCSMM_Hydrocarbures", name: "Hydrocarbures" },
  { dcsmm: "remed_dechets_DCSMM_thesaurus_Peinture", name: "Peinture" },
  { dcsmm: "remed_dechets_DCSMM_Couches", name: "Couches" },
  {
    dcsmm: "remed_dechets_DCSMM_Serviettes_hygieniques",
    name: "Serviettes hygiéniques (dont emballage)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Tampons_et_applicateurs",
    name: "Tampons et applicateurs",
  },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_bouteilles_et_cartouches_de_protoxyde_d_azote_-_no2",
    name: "Bouteilles et cartouches de protoxyde d'azote - NO2",
  },
  { dcsmm: "remed_dechets_DCSMM_Sacs_en_papier", name: "Sacs en papier" },
  {
    dcsmm: "remed_dechets_DCSMM_Sacs_a_dos",
    name: "Sacs à dos et sac en tissu",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Sacs_en_toile_de_jute",
    name: "Sacs en toile de jute",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Filets_a_fruits_et_legumes",
    name: "Filets à fruits et légumes",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Petits_sacs_plastique",
    name: "Petits sacs plastique",
  },
  { dcsmm: "remed_dechets_DCSMM_Sacs_a_crotte", name: "Sacs à crotte" },
  { dcsmm: "remed_dechets_DCSMM_Sacs_de_sel", name: "Sacs de sel" },
  {
    dcsmm: "remed_dechets_DCSMM_Sacs_d_engrais",
    name: "Sacs en plastique épais (ex: engrais)",
  },
  { dcsmm: "remed_dechets_DCSMM_Sacs_plastique", name: "Sacs plastique" },
  {
    dcsmm: "remed_dechets_DCSMM_Sacs_poubelle",
    name: "Sacs poubelle (dont liens)",
  },
  { dcsmm: "remed_dechets_DCSMM_Serres_packs", name: "Serres-packs" },
  {
    dcsmm: "remed_dechets_DCSMM_Souches_de_distribution",
    name: "Souches de distribution",
  },
  { dcsmm: "remed_dechets_DCSMM_Scelles", name: "Scellés" },
  { dcsmm: "remed_dechets_DCSMM_Seringues", name: "Seringues" },
  {
    dcsmm:
      "remed_dechets_DCSMM_thesaurus_skis_et_chaussures_de_ski_ou_morceaux",
    name: "Skis et chaussures de ski (ou morceaux)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_sticks_a_levre",
    name: "Sticks à lèvre",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_tapis_de_protection_pylône",
    name: "Tapis de protection pylône",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_tapis_de_remontee_mecanique",
    name: "Tapis de remontée mécanique",
  },
  { dcsmm: "remed_dechets_DCSMM_Telephones", name: "Téléphones" },
  { dcsmm: "remed_dechets_DCSMM_Tetines", name: "Tétines" },
  { dcsmm: "remed_dechets_DCSMM_Autres_tetrapacks", name: "Autres tetrapacks" },
  { dcsmm: "remed_dechets_DCSMM_Tetrapacks_lait", name: "Tetrapacks lait" },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_textiles_ski_vetements_gants_bonnet",
    name: "Textiles ski (vêtements, gants, bonnet...)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Tirettes_et_capsules",
    name: "Tirettes et capsules",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Tissus_d_ameublement",
    name: "Tissus d'ameublement",
  },
  { dcsmm: "remed_dechets_DCSMM_Toiles_et_voiles", name: "Toiles et voiles" },
  { dcsmm: "remed_dechets_DCSMM_thesaurus_trottinettes", name: "Trottinettes" },
  { dcsmm: "remed_dechets_DCSMM_Tuyaux", name: "Tuyaux souples" },
  {
    dcsmm: "remed_dechets_DCSMM_Vaisselles_verre_et_ceramique",
    name: "Vaisselles (verre et céramique)",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Vaisselles_en_metal",
    name: "Vaisselles en métal",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Assiette_et_plat_en_plastique",
    name: "Assiettes et plats en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Couverts_en_plastique",
    name: "Couverts en plastique",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Vaisselles_en_polystyrene",
    name: "Plats et plateau en polystyrène",
  },
  {
    dcsmm: "remed_dechets_DCSMM_thesaurus_Touillette_et_agitateurs",
    name: "Touillettes et agitateurs",
  },
  {
    dcsmm: "remed_dechets_DCSMM_Vaisselles_en_plastique",
    name: "Vaisselles en plastique",
  },
  { dcsmm: "remed_dechets_DCSMM_thesaurus_velos", name: "Vélos" },
  { dcsmm: "remed_dechets_DCSMM_Vetements_et_chiffons", name: "Vêtements" },
];
