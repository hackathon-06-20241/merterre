export const TAB_PROJETS_ENVERGURE = [
  { nom_ee: "Opération Zéro Déchet des Dunes de Flandre", id: 39 },
  { nom_ee: "STOP Plastique", id: 38 },
  { nom_ee: "Projet Sentinelle", id: 37 },
  { nom_ee: "Clean The Silver Coast", id: 36 },
  { nom_ee: "Les Sentinelles Bleues", id: 35 },
  { nom_ee: "Nettoyons le Sud", id: 34 },
  { nom_ee: "Love ta Bonne Mère", id: 33 },
  {
    nom_ee:
      "Plages vivantes... sans déchet ! [Festival Tous Sentinelles Normandie]",
    id: 32,
  },
  { nom_ee: "Opération Zépal [Zéro Plastik An Loséan]", id: 26 },
  { nom_ee: "World Cleanup Day", id: 25 },
  { nom_ee: "Pirates du Plastique", id: 18 },
  { nom_ee: "Mission CorSeaCare", id: 16 },
  { nom_ee: "Verdon Propre", id: 15 },
  { nom_ee: "Provence Propre", id: 6 },
  { nom_ee: "Rivières Propres", id: 5 },
  { nom_ee: "Montagne Zéro Déchet", id: 4 },
  { nom_ee: "Calanques Propres", id: 1 },
];
