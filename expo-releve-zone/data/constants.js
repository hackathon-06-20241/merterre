import * as FileSystem from "expo-file-system";

export const DATA_STORAGE_PATTERN = "releveData_";
// Define the directory (documentDirectory is safe for app-specific files)
export const DIRECTORY_URI = FileSystem.documentDirectory + "dirReleveData";

export const UNITS = {
  liter: "L",
  percentage: "%",
  kilo: "kg",
};
export const DEFAULT_UNIT = UNITS.liter;

export const removeAccents = (str) => {
  const newStr = str
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase();
  //console.log("newStr:" + newStr);
  return newStr;
};

export function getOppositeUnit(unit) {
  return unit == UNITS.liter ? UNITS.percentage : UNITS.liter;
}

export function checkIsFieldEmpty(field) {
  return field === null || field === undefined || field === "";
}

/**
 * Fonction qui normalize une chaine de caractère =>
 * Mise en minuscule et suppression des accents
 * @param {*} str
 * @returns
 */
export function normalizeString(str) {
  return str
    ?.toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "");
}

export const listeMateriaux = {
  Plastique: "Plastique",
  Caoutchouc: "Caoutchouc (pneu...)",
  Bois: "Bois manufacturé",
  Textile: "Textile",
  Carton: "Papier/Carton",
  Metal: "Métal",
  Verre: "Verre/Céramique",
  Unknown: "Déchets non caractérisés",
};

export const NAV_HOME = "Home";
export const NAV_GENERAL_INFO = "GeneralInfo";
export const NAV_PROTOCOL_CHOICE = "ProtocolChoice";
export const NAV_COUNT_WASTES = "CountWastes";
export const NAV_VOLUME_BAGS = "VolumeBags";
export const NAV_WEIGHT_BAGS = "WeightBags";
export const NAV_MAIN_WASTES = "MainWastes";
export const NAV_COUNT_BRANDS = "CountBrands";
export const NAV_STOCK_EVENT = "StockEvent";
export const NAV_FINALIZE_EXPORT = "FinalizeExport";
