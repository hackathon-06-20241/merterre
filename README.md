# MerTerre

## Description du projet

🌊 Projet Mer Terre - une solution a été développée pour permettre aux bénévoles de l'Association MerTerre de saisir directement les données de collecte de déchets dans leur base de données. Une avancée majeure pour optimiser la gestion des déchets et protéger notre environnement !

## Requirements

## Objectifs & Livrables

Fournir un POC à l'association Mer Terre qui leur permettra de continuer sur une solution opérationelle

## Initial RoadMap

14/06-16/06 => HACKATHON sur Marseille

## Collaboration

Toutes les nouvelles fonctionnalités sont listées dans les "Issues" de Gitlab, chacun peut contribuer en s'attribuant une issue et en créant une branche associée (avec le même nom que l'issue)

## Maquette réalisée lors du HACKATHON

https://miro.com/app/board/uXjVK75tkYM=/

## Organisation du repo

### backend-swagger

Contient un POC de serveur en Node JS avec des fonctionnalité essentielles.
Ce POC met à disposition un certain nombre d'API JSON destiné à un front

Lancement du serveur:

    npm start

### font-bubble-nocode

Pour le moment la licence n'est accessible que pour un seul utilisateur, cela pourra être revu par la suite

### front-react-native

Contient un POC d'une application react native.
Tout d'abord il faut installer les librairies connexes

    npm install

Puis lancement comme ceci (si votre système android est bien configuré)

    npm run android

ou

    npm run ios (si vous êtes sous Mac)

## Infos connexes

Site principale pour la collecte des déchets :

https://www.zero-dechet-sauvage.org/

Sharepoint Zero Mer Terre partagé pour le projet :

https://merterre13.sharepoint.com/:f:/s/TeamMerTerre/EoKBK37899lKnlUGjAizS-kBn4UpZ9Kcsiz6b9i7Mlcw8w?e=nUhS0l